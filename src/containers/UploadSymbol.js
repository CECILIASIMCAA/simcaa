import React, { Component, Fragment } from 'react'
import { Button, Segment, Dropdown, Modal, Image, Form, Grid, Message, Confirm, Icon, Popup, Dimmer, Loader } from 'semantic-ui-react'
import Dropzone from 'react-dropzone'
import { translate } from 'react-i18next'
import { connect } from 'react-redux'

import { apolloFetch } from '../store/apolloFetchUtils'
import CardLayout from '../components/CardLayout'


class UploadSymbol extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalOpen: false,
            confirmOpen: false,
            classOptions: [],
            styleOptions: [],
            colorOptions: [{value: 1, text: props.t("UPSY_IMGCOLOR_BW")}, {value: 2, text: props.t("UPSY_IMGCOLOR_COL")}],
            file: [{preview: '', size: 0}],
            card: this.props.card ? this.props.card : {id: 0, lemma: 'Custom Images', img: 'simcaa.png'},
            tmpFolder: '',
            urlImg: window.env.PathImages,
            imgStat: {width: 0, height: 0, size: 0},
            word: this.props.card ? this.props.card.lemma || this.props.card.voice_master : '',
            style: 0,
            color: 1,
            class: 0,
            privatePhoto: 0,
            shareOptions: [{value: 0, text: props.t("PRLD_DROP_PRIVATE")}, {value: 1, text: props.t("PRLD_DROP_PRIVACY")}],
            error: {hidden: true, text: ''},
            imageLoading: false,
        }
    }

    componentDidMount() {
        // Fetch delle classi
        let localOptionsClass = this.props.classData.slice()
        let localClass
        for (let i = 0; i < localOptionsClass.length; i++) {
            localOptionsClass[i] = {value: localOptionsClass[i].id, text: localOptionsClass[i].descclass}
            if (localOptionsClass[i].text === 'altro') {
                localClass = localOptionsClass[i].value
            }
        }

        // Fetch degli stili
        let localOptionsStyle = this.props.styleData.slice()
        let localStyle
        for (let i = 0; i < localOptionsStyle.length; i++) {
            localOptionsStyle[i] = {value: localOptionsStyle[i].id, text: localOptionsStyle[i].descstyle}
            if (localOptionsStyle[i].text === 'Indefinito') {
                localStyle = localOptionsStyle[i].value
            }
        }

        // Add share option if the user has the right permission
        let localShare = this.state.shareOptions.slice()
        if (this.props.user.user_permissions.can_manage_preload && localShare.length === 2) {
            localShare.push({value: 2, text: this.props.t("PRLD_DROP_TEAM")})
            if (this.props.user.user_permissions.can_manage_staff) {
                localShare.push({value: 3, text: this.props.t("PRLD_DROP_STAFF")})
            }
        }

        this.setState({classOptions: localOptionsClass,
                    styleOptions: localOptionsStyle,
                    class: localClass,
                    style: localStyle,
                    shareOptions: localShare
        })
    }

    componentDidUpdate(prevProps, prevState) {
        let localStat = this.state.imgStat
        let testImg = document.getElementById('imgTmp')
        let self = this
        let imgName = testImg !== null ? testImg.src.split("/").pop() : ''
        if (testImg !== null && imgName !== prevState.card.img) {
            var interval = setInterval(() => {
                // Riprendo i dati dalla div ad ogni ciclo
                testImg = document.getElementById('imgTmp')
                let complete = testImg.complete

                if (complete === true) {
                    // Aggiorno in tempo reale le info sull'immagine
                    localStat.width = testImg.width
                    localStat.height = testImg.height
                    localStat.size = this.state.file[0].size
                    self.setState({imgStat: localStat})
                     clearInterval(interval)
                }
            }, 100)
        }
    }

    // onDrop dell'immagine
    onDrop(files) {
        if (files.length === 0) {
            this.setState({error: {hidden: false, text: this.props.t("ERR_FILESIZE_EXCEED_FORMAT")}})
        } else {
            let localImgStat = this.state.imgStat
            localImgStat.size = files[0].size
            this.setState({file: files, error: {hidden: true, text: ''}, imgStat: localImgStat}, () => {
                this.getPreviewImage()
            })
        }
    }

    // Get full preview
    getPreviewImage() {
        let imgToSend = new FormData()
        imgToSend.append('img',this.state.file[0])
        imgToSend.append('custom', true)

        // let request = new Request(window.env.ApiImageUpload,{
        let request = new Request(window.env.ApiSymbolUpload,{
            method : 'POST',
            mode: 'cors',
            mimeType: 'multipart/form-data',
            body: imgToSend
        })

        this.setState({imageLoading: true})

        fetch(request)
            .then((response) => {return response.json()})
            .then((data) => {
                if (data.code === 1) {
                    // TODO: change error text to match language
                    let textError = data.error + ' associata a queste parole: ' + data.duplicates
                    this.setState({error: {hidden: false, text: textError}, imageLoading: false})
                } else {
                    let localCard = this.state.card
                    localCard.img = data.NewName

                    let localImgStat = this.state.imgStat
                    localImgStat.width = data.OriginalWidth
                    localImgStat.height = data.OriginalHeight
                    this.setState({card: localCard,
                            urlImg: window.env.TmpImage + data.time + '/',
                            tmpFolder: data.time,
                            imgStat: localImgStat,
                            imageLoading: false
                        })
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }

    // Handle open/close of the modal
    openCloseModal() {
        if (this.state.modalOpen === true && this.props.mode === 'upload') {
            this.setState({modalOpen: !this.state.modalOpen,
                        file: [{preview: '', size: 0}],
                        imgStat: {width: 0, height: 0, size: 0},
                        word: '',
                        privatePhoto: 0,
                        error: {hidden: true, text: ''},
                        urlImg: window.env.PathImages,
                        card: {id: 0, lemma: 'Custom Images', img: 'simcaa.png'}
            }, () => this.componentDidMount())
        } else if (this.state.modalOpen === true && this.props.mode === 'save') {
            this.setState({modalOpen: !this.state.modalOpen,
                        word: this.state.card.lemma,
                        privatePhoto: 0,
                        error: {hidden: true, text: ''},
            }, () => this.componentDidMount())
        }
        else {
            this.setState({modalOpen: !this.state.modalOpen, confirmOpen: false,
                        word: this.props.card ? this.props.card.lemma : '',
                        card: this.props.card ? this.props.card : this.state.card,
                        style: this.props.mode === 'edit' ? this.props.card.idstyle : this.state.style,
                        color: this.props.mode === 'edit' ? parseInt(this.props.card.imgcolor, 10) : this.state.color,
                        class: this.props.mode === 'edit' ? this.props.card.idclass : this.state.class,
                    })
        }

    }

    // Handle mode
    handleMode() {
        if (this.props.mode === 'edit') {
            this.edit()
        } else if (this.props.mode === 'delete') {
            this.delete()
        } else {
            this.openCloseConfirm()
        }
    }

    // Handle open/close Confirm
    openCloseConfirm() {
        this.setState({confirmOpen: !this.state.confirmOpen})
    }

    // Handle lemma change input
    wordChange(e) {
        this.setState({word: e.target.value})
    }

    // Handle Dropdown form (style/color/class)
    dropdownChange(type, e, data) {
        if(type === 'style') {
            this.setState({style: data.value})
        } else if (type === 'color') {
            this.setState({color: data.value})
        } else if (type === 'class') {
            this.setState({class: data.value})
        } else if (type === 'share') {
            this.setState({privatePhoto: data.value})
        }
    }

    // Main Upload or Save
    upload() {
        let idlang = 4
        let voice_human = this.state.word.trim().toLowerCase()
        let voice_master = voice_human.replace(/ .*/,'')
        let lexical_expr = voice_human.indexOf(' ') >= 0 ? 1 : 0
        let idclass = this.state.class
        let idcateg = 0
        let type_img = this.state.privatePhoto
        let imgcolor = this.state.color
        let idstyle = this.state.style
        let created_by = this.props.user.id
        let symbol_sign = this.state.card.img

        let with_image = this.state.card.custom === true || this.props.mode === 'upload' ? 1 : 0

        let extraQuery = ''
        if (this.props.mode === 'upload') {
            let tmpFolder = this.state.tmpFolder
            let completeImgPath = tmpFolder + '/' + this.state.card.img
            extraQuery = ', tmp_folder: "' + tmpFolder +'", full_img_path: "' + completeImgPath + '"'
        }


        let query = `
        mutation InsertNewPreloadHeardword {
            createCaaPreloadHeadword(idlang: ${idlang}, voice_master: "${voice_master}",
            voice_human: "${voice_human}", lexical_expr: ${lexical_expr},
            idclass: ${idclass}, idcateg: ${idcateg}, type_img: ${type_img},
            with_image: ${with_image}, idstyle: ${idstyle}, symbol_sign: "${symbol_sign}",
            idteam: ${this.props.idteam}, idstaff: ${this.props.user.idstaff}
            imgcolor: "${imgcolor}", created_by: ${created_by}${extraQuery}) {
                id
            }
        }
        `

        if (voice_human.length === 0) {
            let localError = this.state.error
            localError.hidden = false
            localError.text = this.props.t('ERR_UPLOAD_SYMBOL')
            this.setState({error: localError, confirmOpen: !this.state.confirmOpen})
        } else {
            apolloFetch({ query })
            .then((data) => {
                this.openCloseModal()
            })
            .catch((error) => {
                console.log(error);
            })
        }
    }

    // Check for duplicates and call upload
    save() {
        // Variables GraphQL
        let voice_human = this.state.word.trim().toLowerCase()
        let voice_master = voice_human.replace(/ .*/,'')
        let symbol_sign = this.props.card.img

        let query = `
        query CheckDuplicates {
            query_view (voice_master: "${voice_master}", limit: 100){
                data {
                    voice_master
                    voice_human
                    symbol_sign
                }
            }
            preload_headword(voice_master: "${voice_master}") {
                data {
                    voice_human
                    with_image
                    symbol_sign
                }
            }
        }
        `
        apolloFetch({ query })
            .then((data) => {
                let result = data.data.preload_headword.data.concat(data.data.query_view.data)
                let duplicatesFound = false
                for (let i = 0; i < result.length; i++) {
                    if (result[i].voice_human === voice_human && result[i].symbol_sign === symbol_sign) {
                        duplicatesFound = true
                    }
                }

                if (!duplicatesFound) {
                    this.upload()
                } else {
                    this.setState({error: {hidden: false, text: this.props.t("ERR_DUPLICATE_RECORD")}, confirmOpen: !this.state.confirmOpen,})
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }

    // Edit the currend preload record
    edit() {
        let voice_human = this.state.word.trim().toLowerCase()
        let voice_master = voice_human.replace(/ .*/,'')
        let lexical_expr = voice_human.indexOf(' ') >= 0 ? 1 : 0
        let idclass = this.state.class
        let type_img = this.state.privatePhoto
        let imgcolor = this.state.color
        let idstyle = this.state.style

        let query = `
        mutation UpdatePreloadHeardword {
            updateCaaPreloadHeadword(id:${this.state.card.id}, voice_master: "${voice_master}",
            voice_human: "${voice_human}", lexical_expr: ${lexical_expr},
            idclass: ${idclass}, type_img: ${type_img},
            idstyle: ${idstyle}, imgcolor: "${imgcolor}") {
                id
            }
        }
        `
        if (voice_human.length === 0) {
            let localError = this.state.error
            localError.hidden = false
            localError.text = this.props.t('ERR_UPLOAD_SYMBOL')
            this.setState({error: localError, confirmOpen: !this.state.confirmOpen})
        } else {
            apolloFetch({ query })
            .then((data) => {
                this.openCloseModal()
                this.props.refresh()
            })
            .catch((error) => {
                console.log(error);
            })
        }
    }

    // Delete the current preload record
    delete() {
        let query = `
        mutation UpdatePreloadHeardword {
            deleteCaaPreloadHeadword(id:${this.state.card.id}) {
                id
            }
        }
        `
        apolloFetch({ query })
            .then((data) => {
                this.openCloseModal()
                this.props.refresh()
            })
            .catch((error) => {
                console.log(error);
            })
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        // Render trigger modal based on props.type
        let triggerModal
        if (this.props.type === 'dropdown') {
            triggerModal = <Dropdown.Item
                            style={this.props.style}
                            className={'icon-pointer dropdown-item-hover ' + this.props.className}
                            onClick={this.openCloseModal.bind(this)}>
                                {t("HOME_NAVBAR_MANAGE_SYMBOL")}
                        </Dropdown.Item>
        } else if (this.props.type === 'button') {
            triggerModal = <Button
                            style={this.props.style}
                            color={this.props.buttonColor}
                            className={'icon-pointer ' + this.props.className}
                            onClick={this.openCloseModal.bind(this)}>
                                {t(this.props.buttonTranslateText)}
                        </Button>
        } else if (this.props.type === 'icon' && this.props.mode === 'upload') {
            triggerModal =  <Popup
                                trigger={<Icon name='upload' bordered inverted color="teal"
                                    size='large'
                                    className={'icon-pointer ' + this.props.className}
                                    style={this.props.style}
                                    onClick={this.openCloseModal.bind(this)}/>}
                                content={t("POPUP_IMPORT")}
                            />
        } else if (this.props.type === 'icon' && this.props.mode === 'save') {
            triggerModal =  <Popup
                                trigger={<Icon name='save' bordered inverted color="teal"
                                    size='large'
                                    className={'icon-pointer ' + this.props.className}
                                    style={this.props.style}
                                    disabled={this.props.card.lock === 'lock' ? false : true}
                                    onClick={this.openCloseModal.bind(this)}/>}
                                content={t("POPUP_SAVE")}
                            />
        }

        let dropzoneRef
        let imgSrcUrl = this.state.card.custom ? window.env.CustomImage + this.state.card.img : window.env.PathImages + this.state.card.img

        // MAIN RENDER RETURN
        if (this.props.mode === 'delete') {
            return (
                <Modal trigger={triggerModal} open={this.state.modalOpen}>
                    <Modal.Header>{t("UPSY_SELECTIMG")}</Modal.Header>
                    <Modal.Content>
                        {t("UPSY_DELETE_CONFIRM")}
                    </Modal.Content>
                    <Modal.Actions>
                        <Button.Group>
                            <Button positive onClick={this.delete.bind(this)}>{t(this.props.buttonActionTranslateText)}</Button>
                            <Button.Or />
                            <Button negative onClick={this.openCloseModal.bind(this)}>{t("UPSY_CANCEL")}</Button>
                        </Button.Group>
                    </Modal.Actions>
                </Modal>
            )
        }

        return (
            <Modal trigger={triggerModal} open={this.state.modalOpen}>
                <Modal.Header>{t("UPSY_SELECTIMG")}</Modal.Header>
                <Modal.Content>
                    {
                        this.state.imageLoading ?
                            <Dimmer active>
                                <Loader>Caricamento immagine...</Loader>
                            </Dimmer>
                        :
                        <Fragment>
                            <Message negative hidden={this.state.error.hidden}>
                                <Message.Header>{t("ERR_UPLOAD_SYMBOL_HEADER")}</Message.Header>
                                <p>{this.state.error.text}</p>
                            </Message>
                            <Form>
                                <Form.Field>
                                    <label>{t("UPSY_WORD")}</label>
                                    <input placeholder={t("UPSY_CLASS")} value={this.state.word} onChange={this.wordChange.bind(this)} />
                                </Form.Field>
                                <Form.Field width={6}>
                                    <label>{t("UPSY_CLASS")}</label>
                                    <Dropdown placeholder='Select Class' selection
                                        options={this.state.classOptions}
                                        value={this.state.class}
                                        onChange={this.dropdownChange.bind(this, 'class')}
                                    />
                                </Form.Field>
                                <Form.Field width={6}>
                                    <label>{t("UPSY_IMGTYPE")}</label>
                                    <Dropdown placeholder={t("UPSY_IMGTYPE")} selection
                                        options={this.state.shareOptions}
                                        value={this.state.privatePhoto}
                                        onChange={this.dropdownChange.bind(this, 'share')}
                                    />
                                </Form.Field>
                                <Form.Field width={6}>
                                    <label>{t("UPSY_IMGCOLOR")}</label>
                                    <Dropdown placeholder='Image Color' selection
                                        options={this.state.colorOptions}
                                        value={this.state.color}
                                        onChange={this.dropdownChange.bind(this, 'color')}
                                    />
                                </Form.Field>
                                <Form.Field width={6}>
                                    <label>{t("UPSY_IMGSTYLE")}</label>
                                    <Dropdown placeholder={t("UPSY_IMGCOLOR")} selection
                                        options={this.state.styleOptions}
                                        value={this.state.style}
                                        onChange={this.dropdownChange.bind(this, 'style')}
                                    />
                                </Form.Field>
                                {
                                    this.props.mode !== 'upload' ?
                                    <Form.Field>
                                        <label>Image</label>
                                        <Image size='small' src={imgSrcUrl}/>
                                    </Form.Field>
                                    :
                                    <Fragment>
                                        <Dropzone ref={(node) => { dropzoneRef = node; }}
                                            accept="image/jpeg, image/png"
                                            style={{'display': 'none'}}
                                            onDrop={this.onDrop.bind(this)}
                                            maxSize={5000000}
                                            />
                                        <Button onClick={() => { dropzoneRef.open() }}>
                                            {t("UPSY_OPENDIALOG")}
                                        </Button>
                                        <Image hidden src={this.state.urlImg + this.state.card.img} id='imgTmp' />
                                        <br />
                                        <Segment vertical>{t("UPSY_WIDTH")}: {this.state.imgStat.width} px</Segment>
                                        <Segment vertical>{t("UPSY_HEIGHT")}: {this.state.imgStat.height} px</Segment>
                                        <Segment vertical>{t("UPSY_SIZE")}: {this.state.imgStat.size} byte</Segment>
                                        <Grid padded>
                                            <Grid.Row columns='equal'>
                                                <Grid.Column>
                                                    <CardLayout
                                                        Card={this.state.card}
                                                        isTypo={false}
                                                        imgFullWidth={false}
                                                        mode={true}
                                                        posInput= 'bottom'
                                                        sizeInput= 'small'
                                                        styleInput= 'normal'
                                                        formatInput= 'freeInput'
                                                        colorTextInput= '#000000'
                                                        colorBackgroundInput= '#FFFFFF'
                                                        weightInput= 'weightNormalInput'
                                                        decorationInput= 'decorationNormalInput'
                                                        fontStyleInput= 'styleNormalInput'
                                                        imgSize= 'small'
                                                        imgPadding= 'imgpadding1'
                                                        urlImg= {this.state.urlImg}
                                                    />
                                                </Grid.Column>
                                                <Grid.Column>
                                                    <CardLayout
                                                        Card={this.state.card}
                                                        isTypo={false}
                                                        imgFullWidth={false}
                                                        mode={true}
                                                        posInput= 'bottom'
                                                        sizeInput= 'small'
                                                        styleInput= 'normal'
                                                        formatInput= 'freeInput'
                                                        colorTextInput= '#000000'
                                                        colorBackgroundInput= '#FFFFFF'
                                                        weightInput= 'weightNormalInput'
                                                        decorationInput= 'decorationNormalInput'
                                                        fontStyleInput= 'styleNormalInput'
                                                        imgSize= 'tiny'
                                                        imgPadding= 'imgpadding1'
                                                        urlImg= {this.state.urlImg}
                                                    />
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Fragment>
                                }
                            </Form>
                            <Message negative hidden={this.state.error.hidden}>
                                <Message.Header>{t("ERR_UPLOAD_SYMBOL_HEADER")}</Message.Header>
                                <p>{this.state.error.text}</p>
                            </Message>
                            <Confirm
                                open={this.state.confirmOpen}
                                header='Upload Guide'
                                content={t("HOWTO_UPLOAD_SYMBOL").split('\n').map((line, key) => <p key={key}>{line}</p>)}
                                onConfirm={this.props.mode === 'save' ? this.save.bind(this) : this.upload.bind(this)}
                                onCancel={this.openCloseConfirm.bind(this)}
                            />
                        </Fragment>
                    }
                </Modal.Content>
                <Modal.Actions>
                    <Button.Group>
                        <Button positive onClick={this.handleMode.bind(this)}>{t(this.props.buttonActionTranslateText)}</Button>
                        <Button.Or />
                        <Button negative onClick={this.openCloseModal.bind(this)}>{t("UPSY_CANCEL")}</Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        styleData: state.styleData,
        classData: state.classData,
        idteam: state.filterValue,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(UploadSymbol))
