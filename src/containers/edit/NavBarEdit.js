import React, { Component } from 'react'
import { Segment, Button, Breadcrumb, Modal } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import { translate } from 'react-i18next'

import { connect } from 'react-redux'
import { saveProject, CardUIcopyPaste } from '../../store/actions/CardUIActions'
import { chapterDataUpdateData, chapterContentDataFetchDataSuccess, chapterRowsDataFetchDataSuccess } from '../../store/actions/ChapterFetchActions'

class NavBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openConfirm: false,
            url: '/',
        }
    }

    // Open Confirm to exit
    openConfirm(url, e){
        this.setState({openConfirm: !this.state.openConfirm, url})
    }

    // Triggera il salvataggio del progetto
    triggerSaveProject(page = null, e) {
        if (page) {
            this.redirectChapter(page)
        }
        this.props.saveProject(this.props.match.params.chapterid, page)
    }

    toTypoView() {
        let size = this.getSizeCard()
        sessionStorage.setItem('cardWidth', size.width)
        sessionStorage.setItem('cardHeight', size.height)
        let self = this
        var pushToPage = setInterval(() => {
            if (self.props.saveMessage && self.props.match.params.mode === 'edit') {
                setTimeout(() => {
                    self.props.history.push('/layout/edit/' + self.props.match.params.projectid + '/' + self.props.match.params.chapterid)
                }, 1000)
                clearInterval(pushToPage)
            } else if(self.props.match.params.mode === 'view'){
                self.props.history.push('/layout/view/' + self.props.match.params.projectid + '/' + self.props.match.params.chapterid)
                clearInterval(pushToPage)
            }
        }, 100)
    }

    // Get dimension of cards
    getSizeCard() {
        let numCard = document.getElementsByClassName('cardUI');
        let cardHeight = document.getElementById('card-0').offsetHeight
        let cardWidth = []
        for (let i = 0; i < numCard.length; i++) {
            cardWidth[i] = document.getElementById(`card-${i}`).offsetWidth
        }
        return {width: cardWidth, height: cardHeight}
    }

    // Sblocca il capitolo alla sua chiusura
    redirectChapter(url, e) {
        if (this.props.match.params.mode === 'edit') {
            if (url !== 'typo') {
                let query = `
                    mutation UnlockChapter {
                        updateCaaChapter(id: ${this.props.match.params.chapterid}, chapt_user_block: 0) {
                            id
                        }
                    }
                `
                this.props.unlockChapter(query)
                let self = this
                var pushToPage = setInterval(() => {
                    if (!self.props.chapterIsLoading && !self.props.chapterHasErrored) {
                        setTimeout(() => {
                            self.resetContent([])
                            self.props.history.push(url)
                        }, 1000)
                        clearInterval(pushToPage)
                    }
                }, 100)
            } else {
                this.toTypoView()
            }
        } else if (this.props.match.params.mode === 'view') {
            if (url !== 'typo') {
                this.resetContent([])
                this.props.history.push(url)
            } else {
                this.toTypoView()
            }
        }
    }

    resetContent() {
        this.props.resetContent([])
        this.props.resetCopyCard({})
        this.props.resetRowsCard([])
    }

    render() {
        const { t } = this.props
        let mode = this.props.checked ? t("HEAD_BTN_VIEW") : t("HEAD_BTN_EDIT")

        if (this.props.match.params.mode === 'view') {
            return (
                <Segment basic style={{'padding': '0px'}} id='navbar-edit'>
                    <Button color='red' onClick={this.redirectChapter.bind(this, '/project/' + this.props.match.params.projectid)}>{t("HEAD_BTN_CLOSE")}</Button>
                    <Button color='violet' onClick={this.toTypoView.bind(this)}>
                        {t("HEAD_BTN_TYPO")}
                    </Button>
                </Segment>
            )
        } else {
            return (
                <Segment basic style={{'padding': '0px'}} id='navbar-edit'>
                    <Button color='green'
                        loading={this.props.chapterIsLoading}
                        disabled={this.props.chapterIsLoading}
                        onClick={this.triggerSaveProject.bind(this, null)}
                    >
                        {t("HEAD_BTN_SAVE")}
                    </Button>
                    <Button color='orange'
                        loading={this.props.chapterIsLoading}
                        disabled={this.props.chapterIsLoading}
                        onClick={this.triggerSaveProject.bind(this, '/project/' + this.props.match.params.projectid)}
                    >
                        {t("HEAD_BTN_SAVECLOSE")}
                    </Button>
                    <Button color='red'
                        loading={this.props.chapterIsLoading}
                        disabled={this.props.chapterIsLoading}
                        onClick={this.openConfirm.bind(this, '/project/' + this.props.match.params.projectid)}
                    >
                        {t("HEAD_BTN_CLOSE")}
                    </Button>
                    <Button color='blue' onClick={this.props.checkMode}>{mode}</Button>
                    <Button color='violet'
                        loading={this.props.chapterIsLoading}
                        disabled={this.props.chapterIsLoading}
                        onClick={this.triggerSaveProject.bind(this, 'typo')}
                    >
                        {t("HEAD_BTN_TYPO")}
                    </Button>
                    <Modal open={this.state.openConfirm} onClose={this.close}>
                        <Modal.Content>
                            {t("CLS_MESSAGE")}
                        </Modal.Content>
                        <Modal.Actions>
                            <Button.Group>
                                <Button negative
                                    onClick={this.openConfirm.bind(this)}
                                    loading={this.props.chapterIsLoading}
                                    disabled={this.props.chapterIsLoading}
                                >
                                    {t("DELETE_CNF_CANCEL")}
                                </Button>
                                <Button.Or/>
                                <Button positive
                                    onClick={this.redirectChapter.bind(this, this.state.url)}
                                    loading={this.props.chapterIsLoading}
                                    disabled={this.props.chapterIsLoading}
                                >
                                    {t("HEAD_BTN_CLOSENOSAVE")}
                                </Button>
                            </Button.Group>
                        </Modal.Actions>
                    </Modal>
                    <Breadcrumb style={{'float': 'right'}}>
                        <Breadcrumb.Section link onClick={this.openConfirm.bind(this, '/')}>home</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section link onClick={this.openConfirm.bind(this, '/project/' + this.props.match.params.projectid)}>{this.props.projName}</Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section active>{this.props.chaptName}</Breadcrumb.Section>
                    </Breadcrumb>
                </Segment>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        saveMessage: state.CardUIsaveIsCompleted,
        chapterIsLoading: state.chapterDataIsLoading,
        chapterHasErrored: state.chapterDataHasErrored,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveProject: (chapterId, page) => dispatch(saveProject(chapterId, page)),
        unlockChapter: (mutation) => dispatch(chapterDataUpdateData(mutation)),
        resetContent: (value) => dispatch(chapterContentDataFetchDataSuccess(value)),
        resetCopyCard: (value) => dispatch(CardUIcopyPaste(value)),
        resetRowsCard: (value) => dispatch(chapterRowsDataFetchDataSuccess(value)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(withRouter(NavBar)))
