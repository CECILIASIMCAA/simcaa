import React, { Component } from 'react'
import { Segment, Button, Dropdown, Image, Message, Loader, Dimmer, Menu, Divider, Label } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { Rnd } from 'react-rnd'
import { Stage, Layer, Rect, Circle, Arrow, Line } from 'react-konva'
import Konva from 'konva'

import { withApolloFetch } from '../../withApolloFetch'
import CardLayout from '../../components/CardLayout'
import CustomImgsDnDUpload from '../../components/typo/CustomImgsDnDUpload'
import DeleteCustomElement from '../../components/typo/DeleteCustomElement'
import Draw from '../../components/typo/Draw'
import AddCustomText from '../../components/typo/AddCustomText'
import Can from '../../containers/Permission'

import { connect } from 'react-redux'
import { chapterTypoDataFetchDataSuccess } from '../../store/actions/ChapterFetchActions'
import { dispatchTypoImgSize } from '../../store/actions/TypoActions'
import TransformerDraw from '../../components/typo/TransformerDraw'

class LayoutExport extends Component {
    constructor(props) {
        super(props)
        this.handler = this.handler.bind(this)
        this.state = {
            savedProject: [],
            profile: {},
            layout: {},
            cards: [],
            text: [],
            shapes: [],
            enableDraw: {rect: false, circle: false, arrow: false, line: false},
            isDrawing: false,
            shapesMode: false,
            drawOptions: {strokeColor: '#000', fillColor: '#fff', strokeWidth: 4},
            selectedShapeName: '',
            classSheet: 'PreviewTypoDocument',
            showGridPage: false,
            imageSize: [{type: 'imgsize', value: 50, text: 'mini'},
                        {type: 'imgsize', value: 80, text: 'tiny'},
                        {type: 'imgsize', value: 120, text: 'small'},
                        {type: 'imgsize', value: 150, text: 'medium'},
                        {type: 'imgsize', value: 300, text: 'large'},
                        {type: 'imgsize', value: 450, text: 'big'},
                        {type: 'imgsize', value: 600, text: 'huge'},
                        {type: 'imgsize', value: 800, text: 'massive'}],
            customSymbolSize: '',
            customImgs: [],
            selectedCustomElement: 0,
            multipleDrag: {on: false, key: ''},
            listMultipleDragIds: [],
            saveMessage: true,
            finishLoad: false,
            loadingButton: false,
            hasTypoSaved: false,
            pagesNumber: [],
            selectedPage: 0,
            menuHeight: 0,
            drawCanvasHeight: 0,
            hiddenBar: {mode: false, style: {'display': 'block', 'margin': '0px'}},
        }
    }

    componentWillMount() {
        let indexProjectArray = this.props.projects.findIndex(x => x.proj_id === this.props.projectID)
        let profile = JSON.parse(this.props.projects[indexProjectArray].proj_profile)
        let stateCard = this.minimizeCard()
        let layout = profile.layout
        let savedTypo = this.props.typoContent ? Object.assign({}, this.props.typoContent) : null

        // Definisco la nuova dimensione dei simboli
        let newSymbolSize = this.state.customSymbolSize !== '' ? this.state.customSymbolSize : this.props.dropdownImgSize

        // Se la typo non è salvata fa tutti i calcoli necessari
        if (savedTypo === null || savedTypo.length === 0) {
            let localRows = this.props.chapterRows.slice()

            // Chunko l'array delle card per in base alle pagine
            let arraySlice = [],
                sliceStart = 0
            for (let i = 0; i < localRows.length; i++) {
                if (localRows[i].page === true) {
                    arraySlice.push([sliceStart, i - 1])
                    sliceStart = i
                }
                if (i === localRows.length - 1) {
                    arraySlice.push([sliceStart, i])
                }
            }
            let chunkedCard = []
            for (let i = 0; i < arraySlice.length; i++) {
                chunkedCard.push([])
                for (let j = 0; j < stateCard.length; j++) {
                    if (arraySlice[i][0] <= stateCard[j].row && stateCard[j].row <= arraySlice[i][1]) {
                        chunkedCard[i].push(stateCard[j])
                    }
                }
            }

            // Prendo il margine desiderato dal layout
            let margins = layout.layout_margins
            Object.keys(margins).forEach(key => margins[key] = parseInt(margins[key], 10))
            let marginWidth = margins.right + margins.left
            let marginHeight = margins.top + margins.bottom

            // Salvo il width delle card
            let localCardWidth = JSON.parse('[' + sessionStorage.getItem('cardWidth') + ']'),
                localCardHeight = parseInt(sessionStorage.getItem('cardHeight'), 10)

            let realPageWidth = layout.width - marginWidth,
                realPageHeight = layout.height - marginHeight,
                heightY = 0,
                widthX = 0,
                page = 0
            for (let i = 0; i < chunkedCard.length; i++) {
                let currentChunk = chunkedCard[i].slice()
                realPageWidth = layout.width - marginWidth
                realPageHeight = layout.height - marginHeight
                for (let j = 0; j < currentChunk.length; j++) {
                    currentChunk[j].page = page + i
                    // Calcolo la coordinata X della card
                    if ((currentChunk[j - 1] && currentChunk[j].row === currentChunk[j - 1].row) || j === 0) {
                        // realPageWidth -= localCardWidth[j] + margins.line_spacing_left
                        realPageWidth -= localCardWidth[currentChunk[j].id] + margins.line_spacing_left
                    } else {
                        // realPageWidth = layout.width - marginWidth - localCardWidth[j]
                        realPageWidth = layout.width - marginWidth - localCardWidth[currentChunk[j].id]
                    }
                    
                    if (realPageWidth < 0) {
                        currentChunk = this.increaseKey(j, currentChunk, 'row')
                        // realPageWidth = layout.width - marginWidth - localCardWidth[j]
                        realPageWidth = layout.width - marginWidth - localCardWidth[currentChunk[j].id]
                    }

                    if (currentChunk[j - 1] && currentChunk[j].row === currentChunk[j - 1].row) {
                        // widthX = currentChunk[j - 1].x + localCardWidth[j - 1] + margins.line_spacing_left
                        widthX = currentChunk[j - 1].x + localCardWidth[currentChunk[j - 1].id] + margins.line_spacing_left
                    } else {
                        widthX = margins.left
                    }
                    currentChunk[j].x = widthX
                    currentChunk[j].lastX = currentChunk[j].x
                    // currentChunk[j].rndWidth = localCardWidth[j]
                    currentChunk[j].rndWidth = localCardWidth[currentChunk[j].id]
                    
                    // Calcolo la coordinata Y della card
                    if ((currentChunk[j - 1] && currentChunk[j].row !== currentChunk[j - 1].row) || j === 0) {
                        realPageHeight -= localCardHeight + margins.line_spacing_top + (margins.new_line * currentChunk[j].times)
                    }
                    if (realPageHeight < 0) {
                        currentChunk = this.increaseKey(j, currentChunk, 'page')
                        realPageHeight = layout.height - marginHeight - localCardHeight - margins.top - (margins.new_line * currentChunk[j].times)
                        page++
                    }
                    
                    heightY = currentChunk[j].row === 0 ? margins.top : margins.top + margins.new_line * currentChunk[j].times
                    if (currentChunk[j - 1] && currentChunk[j].row === currentChunk[j - 1].row) {
                        heightY = currentChunk[j - 1].y
                    } else if (currentChunk[j - 1] && currentChunk[j].page === currentChunk[j - 1].page) {
                        heightY = currentChunk[j - 1].y + localCardHeight + margins.line_spacing_top + (margins.new_line * currentChunk[j].times)
                    } else {
                        heightY = margins.top + (margins.new_line * currentChunk[j].times)
                    }
                    currentChunk[j].y = heightY
                    currentChunk[j].lastY = currentChunk[j].y
                    currentChunk[j].rndHeight = localCardHeight
                }
                chunkedCard[i] = currentChunk
            }

            // Setta il numero di pagine
            let pagesNumber=[];
            for (let i = 0; i < stateCard.length; i++) {
              pagesNumber.push(stateCard[i].page);
            }
            pagesNumber=[...new Set(pagesNumber)]
            this.setState({savedProject: this.props.chapterContent, cards: stateCard, selectedPage: 0,
                            profile, layout, customSymbolSize: newSymbolSize, pagesNumber})
        } else {
            let typoCard = savedTypo.card
            let typoImg = savedTypo.img
            let typoText = savedTypo.text
            let typoShapes = savedTypo.shapes
            let pagesNumber = savedTypo.pagesNumber
            newSymbolSize = savedTypo.imgSize
            this.setState({savedProject: this.props.chapterContent, cards: typoCard, text: typoText, selectedPage: 0, shapes: typoShapes,
                            customImgs: typoImg, profile, layout , customSymbolSize: newSymbolSize, hasTypoSaved: true, pagesNumber})
        }
        this.expandInputAll()
        setTimeout(() => {
            this.forceUpdate()
        }, 2000)
    }

    componentDidMount() {
        let nCard = document.getElementsByClassName('uicardlayout')
        if (nCard.length > 0 && nCard.length === this.props.chapterContent.length) {
            for (let i = 0; i < nCard.length; i++) {
                if (this.state.cards[i].resized === true) {
                    let img = document.getElementById('layout-' + i).getElementsByClassName('image')
                    Object.values(img).forEach(el => el.classList.add('imgFullWidth'))
                }
            }
        }

        this.setImageCSS()

        let menuHeight = document.getElementById('typo-menu')
        if (menuHeight) {
            setTimeout(() => {
                this.setState({menuHeight: menuHeight.offsetHeight})
                let convaElement = document.getElementsByClassName('konvajs-content')
                if (convaElement && convaElement[0]) {
                    convaElement[0].style.top = menuHeight.offsetHeight + 20 + 'px'
                }
            }, 1000)
        }
    }

    // Distanzia le card se serve
    componentDidUpdate() {
        this.setImageCSS()

        let pageWrapper = document.getElementById('pageWrapper')
        if (pageWrapper && this.state.drawCanvasHeight !== pageWrapper.offsetHeight - 19) {
            this.setState({drawCanvasHeight: pageWrapper.offsetHeight - 19})
        }

        // Setta l'altezza giusta per le pagine
        let page = document.getElementsByClassName('PreviewTypoDocument')
        for (var i = 0; i < page.length; i++) {
            page[i].setAttribute('height', this.state.layout.height * this.state.pagesNumber.length + 'px', '!important')
        }
    }

    // Aumenta le righe/pagine dell'array delle card
    increaseKey(start, array, key) {
        for (let i = start; i < array.length; i++) {
            array[i][key]++
        }
        return array
    }

    setImageCSS() {
        if (this.state.finishLoad === false) {
            let self = this
            setTimeout(() => {
                let imgDivs = document.getElementsByClassName('image')
                for (let i = 0; i < imgDivs.length; i++) {
                    imgDivs[i].style.setProperty("display", "block", "important")
                    imgDivs[i].style.setProperty("margin", "auto", "important")
                }
                self.setState({finishLoad: true})
            }, 0)
        }
    }

    expandInputAll(globalWidth = null) {
        if (!globalWidth) {
            globalWidth = sessionStorage.getItem('cardWidth').split(',')
        }
        let localCard = this.state.cards
        for (var i = 0; i < localCard.length; i++) {
            let inputResize = document.getElementById('textLayout-' + i)
            if (inputResize) {
                inputResize.style.width = '0px'
                if (inputResize.scrollWidth > inputResize.clientWidth) {
                    inputResize.style.width = inputResize.scrollWidth + 30 + 'px'
                    globalWidth[i] = inputResize.scrollWidth + 30
                }
            }
        }
        sessionStorage.setItem('cardWidth', globalWidth)
    }

    // Minimize cards and add page/resize properties
    minimizeCard() {
        let arrayCard = this.props.chapterContent.slice()
        
        // add page/resize variable to each card
        for (let i = 0; i < arrayCard.length; i++) {
            let deepCopy = Object.assign({}, arrayCard[i])
            delete deepCopy.imgAlt
            deepCopy.id = i
            deepCopy.page = 0
            deepCopy.resized = false
            deepCopy.times = this.props.chapterRows[deepCopy.row].times
            arrayCard[i] = Object.assign({}, deepCopy)
        }
        
        return arrayCard
    }

    // Change all cards size
    onChangeDropdown(event, data) {
        let currentSelectedSize = this.state.imageSize.find(x => x.value === data.value)
        let localCard = this.state.cards

        let imgCurrentSize = document.getElementById('layout-0').getElementsByTagName('img')[0].offsetWidth
        for (let i = 0; i < localCard.length; i++) {
            let newSize = data.value - imgCurrentSize
            localCard[i].rndWidth += newSize
            localCard[i].rndHeight += newSize
        }

        this.props.setTypoDropdown(currentSelectedSize.text)
        this.setState({cards: localCard, customSymbolSize: currentSelectedSize.text, finishLoad: false}, () => {
            // Ricalcolo le dimensioni delle card
            let numCard = document.getElementsByClassName('uicardlayout');
            let cardHeight = document.getElementById('layout-0').offsetHeight
            let cardWidth = []

            for (let i = 0; i < numCard.length; i++) {
                let inputResize = document.getElementById('textLayout-' + i)
                if (inputResize) {
                    // TODO: RIVEDERE PER IL CALCOLO DELLO SPAZIO --- NOT WORKING (NON ENTRA NELLA IF)
                    // if (inputResize.scrollWidth > inputResize.clientWidth) {
                    //     document.getElementsByClassName('typoCard-' + i)[0].style.width = 'auto'
                    // }
                    cardWidth[i] = document.getElementById('layout-' + i).offsetWidth
                }
            }
            sessionStorage.setItem('cardWidth', cardWidth)
            sessionStorage.setItem('cardHeight', cardHeight)
            this.expandInputAll(cardWidth)
            this.componentWillMount()
        })
    }

    // DnD of cards
    drag(item, e, d) {
        let localCard = this.state.cards
        let localListIds = [...new Set(this.state.listMultipleDragIds)]
        let deltaX = d.x - item.lastX
        let deltaY = d.y - item.lastY
        if (localListIds.indexOf(item.id) > -1) {
            for (let i = 0; i < localListIds.length; i++) {
                let tmpX = localCard[localListIds[i]].lastX + deltaX
                let tmpY = localCard[localListIds[i]].lastY + deltaY
                if (tmpX < 0) {
                    localCard[localListIds[i]].x = 0
                } else if (tmpX + localCard[localListIds[i]].rndWidth > this.state.layout.width) {
                    localCard[localListIds[i]].x = this.state.layout.width - localCard[localListIds[i]].rndWidth
                } else {
                    localCard[localListIds[i]].x = tmpX
                }
                localCard[localListIds[i]].y = tmpY < (-this.state.layout.height * localCard[item.id].page) ? -this.state.layout.height * localCard[item.id].page : tmpY
                localCard[localListIds[i]].lastX = localCard[localListIds[i]].x
                localCard[localListIds[i]].lastY = localCard[localListIds[i]].y
            }
        } else {
            let tmpX = localCard[item.id].lastX + deltaX
            let tmpY = localCard[item.id].lastY + deltaY
            if (tmpX < 0) {
                localCard[item.id].x = 0
            } else if (tmpX + localCard[item.id].rndWidth > this.state.layout.width) {
                localCard[item.id].x = this.state.layout.width - localCard[item.id].rndWidth
            } else {
                localCard[item.id].x = tmpX
            }
            localCard[item.id].y = tmpY < (-this.state.layout.height * localCard[item.id].page) ? -this.state.layout.height * localCard[item.id].page : tmpY
            localCard[item.id].lastX = localCard[item.id].x
            localCard[item.id].lastY = localCard[item.id].y
        }
        this.setState({cards: localCard})
    }

    // Resize of cards
    resize(item, e, direction, ref, delta, position, event) {
        let img = document.getElementById('layout-' + item.id).getElementsByClassName('image')
        // img[0].classList.add('imgFullWidth')
        Object.values(img).forEach(el => el.classList.add('imgFullWidth'))
        let localCard = this.state.cards
        localCard[item.id].rndWidth = ref.offsetWidth
        localCard[item.id].rndHeight = ref.offsetHeight
        localCard[item.id].resized = true
        this.setState({cards: localCard})
    }

    // DnD of custom imgs
    dragImg(item, index, e, d){
        let localImgs = this.state.customImgs
        if (localImgs[index].height === 'auto' || localImgs[index].width === 'auto') {
            localImgs[index].height = document.getElementById('dndImg-' + index).height
            localImgs[index].width = document.getElementById('dndImg-' + index).width
        }
        if (d.x < 0) {
            localImgs[index].x = 0
        } else if (d.x + localImgs[index].width > this.state.layout.width) {
            localImgs[index].x = this.state.layout.width - localImgs[index].width
        } else {
            localImgs[index].x = d.x
        }
        localImgs[index].y = d.y < 0 ? 0 : d.y
        this.setState({customImgs: localImgs})
    }

    // Resize of the custom imgs
    resizeImg(item, index, e, direction, ref, dimensions, position){
        let localImgs = this.state.customImgs
        localImgs[index].height = document.getElementById('dndImg-' + index).height
        localImgs[index].width = document.getElementById('dndImg-' + index).width
        this.setState({customImgs: localImgs})
    }

    // Get the selected/clicked custom image
    selectCustomImg(element, type, e) {
        e.stopPropagation()
        if (type === 'page') {
            if (this.state.selectedCustomElement.type === 'image') {
                let localCustomImgs = this.state.customImgs.slice()
                let index = localCustomImgs.findIndex(img => img === this.state.selectedCustomElement.element)
                if (index >= 0) {
                    document.getElementById('dndImg-' + index).style.border = '0px'
                }
            }
            if (this.state.selectedCustomElement.type === 'text') {
                let localCustomText = this.state.text.slice()
                let index = localCustomText.findIndex(text => text === this.state.selectedCustomElement.element)
                if (index >= 0) {
                    document.getElementById('customText-' + index).style.border = '0px'
                }
            }
        } else if (type === 'text') {
            let localCustomText = this.state.text.slice()
            let index = localCustomText.findIndex(text => text === element)
            if (index >= 0) {
                document.getElementById('customText-' + index).style.border = '5px dashed red'
            }
        } else {
            let localCustomImgs = this.state.customImgs.slice()
            let index = localCustomImgs.findIndex(img => img === element)
            if (index >= 0) {
                document.getElementById('dndImg-' + index).style.border = '5px dashed red'
            }
        }
        this.setState({selectedCustomElement: {type, element}})
    }

    // Modify the z-index of the selected custom image
    modifyZindexElements(direction, e) {
        if (this.state.selectedCustomElement.type === 'image') {   
            let localCustomImgs = this.state.customImgs.slice()
            let index = localCustomImgs.findIndex(img => img === this.state.selectedCustomElement.element)
            if (index >= 0) {
                localCustomImgs[index].zIndex = direction === 'up'? localCustomImgs[index].zIndex + 1 : localCustomImgs[index].zIndex - 1
                this.setState({customImgs: localCustomImgs})
            }
        } else {
            let localText = this.state.text.slice()
            let index = localText.findIndex(text => text === this.state.selectedCustomElement.element)
            if (index >= 0) {
                localText[index].zIndex = direction === 'up'? localText[index].zIndex + 1 : localText[index].zIndex - 1
                this.setState({text: localText})
            }
        }
    }

    // DnD of custom text
    dragText(item, index, e, d){
        let localText = this.state.text
        let width = document.getElementById('customText-' + index).offsetWidth
        if (d.x < 0) {
            localText[index].x = 0
        } else if (d.x + width > this.state.layout.width) {
            localText[index].x = this.state.layout.width - width
        } else {
            localText[index].x = d.x
        }
        localText[index].y = d.y < 0 ? 0 : d.y
        this.setState({text: localText})
    }

    // Resize of the custom text
    resizeText(item, index, e, direction, ref, dimensions, position){
        let localText = this.state.text
        localText[index].height = document.getElementById('customText-' + index).height
        localText[index].width = document.getElementById('customText-' + index).width
        this.setState({text: localText})
    }

    // Handle custom imgs upload
    handler(imgs){
        //handler per ricevere immagini caricate da modale, salvo solo il nome di ciascuna immagine e pos iniziale
        let localImgs = this.state.customImgs
        for (let i = 0; i < imgs.length; i++) {
            let imgWithPos = {
                name: imgs[i].name,
                x: 0,
                y: this.getYPage(),
                height: 'auto',
                width: 'auto',
                old: imgs[i].old,
                zIndex: 99
            }
            localImgs.push(imgWithPos)
        }

        this.setState({customImgs: localImgs})
    }

    // Multiple Drag Set
    setMultipleDrag(e) {
        if (e.shiftKey) {
            this.setState({multipleDrag: {on: true, key: 'shift'}})
        } else if (e.ctrlKey) {
            this.setState({multipleDrag: {on: true, key: 'ctrl'}})
        } else {
            this.setState({multipleDrag: {on: false, key: ''}})
        }
        if (e.shiftKey && e.ctrlKey) {
            let localCard = this.state.cards.slice()
            for (let i = 0; i < this.state.listMultipleDragIds.length; i++) {
                // document.getElementById('layout-' + this.state.listMultipleDragIds[i]).classList.remove('multipleDrag')
                localCard[this.state.listMultipleDragIds[i]].multipleDrag = false
            }
            this.setState({multipleDrag: {on: false, key: ''}, listMultipleDragIds: [], cards: localCard})
        }
    }

    // Controlla se una o più card sono selezionate per il multiple drag
    checkMultipleDrag(item, e) {
        if (this.state.multipleDrag.on === true) {
            let localCard = this.state.cards.slice()
            let localListIds = this.state.listMultipleDragIds
            let ifInList = localListIds.indexOf(item.id)

            let element = document.getElementById(`layout-${item.id}`)
            if (ifInList < 0) {
                if (this.state.multipleDrag.key === 'shift' && localListIds.length === 1) {
                    let minMaxId = [localListIds[0], item.id]
                    for (let i = Math.min(...minMaxId); i <= Math.max(...minMaxId); i++) {
                        localListIds.push(i)
                        localCard[i].multipleDrag = true
                        // document.getElementById('layout-' + i).classList.add('multipleDrag')
                    }
                } else {
                    // element.classList.add('multipleDrag')
                    localCard[item.id].multipleDrag = true
                    localListIds.push(item.id)
                }
            } else {
                // element.classList.remove('multipleDrag')
                localCard[item.id].multipleDrag = false
                localListIds.splice(ifInList, 1)
            }
            // Rimuovo i duplicati degli id se serve
            localListIds = Array.from(new Set(localListIds))
            this.setState({listMultipleDragIds: localListIds, cards: localCard})
        }
    }

    // Save to PDF, NON FUNZIONANTE TODO: DA RIVEDERE/TOGLIERE
    printDocument() {
        // const input = document.getElementById('printable-div');
        // html2canvas(document.body, {allowTaint: true, useCORS: true, proxy:'127.0.0.1'})
        //     .then((canvas) => {
        //         const imgData = canvas.toDataURL('image/png');
        //         const pdf = new jsPDF();
        //         pdf.addImage(imgData, 'png', 0, 0);
        //         // pdf.output('dataurlnewwindow');
        //         // pdf.save("download.pdf");
        //     })

        let htmlToSend = new FormData()
        // let page = document.getElementsByClassName('PreviewTypoDocument')[0].innerHTML
        let page = document.documentElement.outerHTML
        htmlToSend.append('html', page)
        console.log(page);
        let request = new Request('http://10.0.0.132:8085/graphql/convertpdf',{
            method : 'POST',
            mode: 'cors',
            mimeType: 'multipart/form-data',
            body: htmlToSend
        })

        fetch(request)
            .then((response) => {
                return response.blob()
            })
            .then((myBlob) => {
                let pdfURL = URL.createObjectURL(myBlob)

                let a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                a.href = pdfURL;
                a.download = 'simcaa.pdf';
                a.click();
                window.URL.revokeObjectURL(pdfURL);

                window.open(pdfURL);
            })
            .catch((error) => {
                //errore, lo comunico all'utente
                console.log('errore nel pdf ->' + error)
            })
    }

    // Save the current Typo configuration
    saveTypo() {
        const localCard = this.state.cards.map(({multipleDrag, ...cards}) => cards)
        let localProject = { "id":  this.props.match.params.chapterid,
                                "chapt_typo": {'card': localCard, 'img': this.state.customImgs,
                                                'text': this.state.text, 'shapes': this.state.shapes,
                                                'pagesNumber': this.state.pagesNumber,
                                                'imgSize': this.props.dropdownImgSize}}
        localProject.chapt_typo = JSON.stringify(localProject.chapt_typo)
        let url = window.env.RestApiCard
        let data = JSON.stringify(localProject)
        let xhr = new XMLHttpRequest()
        this.setState({loadingButton: true})
        xhr.addEventListener("readystatechange", () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    this.setState({saveMessage: false, loadingButton: false, hasTypoSaved: true})
                    this.sleep(1000).then(() => {
                        this.setState({saveMessage: true})
                    });

                }
            }
        })
        xhr.open("POST", url)
        xhr.setRequestHeader("content-type", "application/json")
        xhr.send(data)
    }

    // Reset the layout to null
    resetTypo(mode = null, e) {
        let query = `
        mutation resetTypo {
            updateCaaChapter (id: ${this.props.match.params.chapterid}, chapt_typo: "null") {
                id
            }
        }
        `
        this.props.apolloFetch({ query })
            .then((data) => {
                this.props.chapterToTypo(null)
                this.props.setTypoDropdown(this.state.profile.imgSize)
                this.componentWillMount()
                let imgDivs = document.getElementsByClassName('imgFullWidth')
                for (let i = 0; i < imgDivs.length; i++) {
                    imgDivs[i].classList.remove("imgFullWidth")
                }
                if (mode === 'full') {
                    this.setState({customImgs: [], hasTypoSaved: false})
                } else {
                    this.setState({hasTypoSaved: false})
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }

    // Add a page
    addPage(){
        let localPagesNumber = this.state.pagesNumber
        localPagesNumber.push(localPagesNumber.length)
        this.setState({pagesNumber: localPagesNumber})
    }

    // Add Text to page
    addText(html, inverted, color) {
        let y = this.getYPage()
        let localText = this.state.text
        localText.splice(localText.length, 0, {text: html,inverted, color, x: 0, y: Math.floor(y), zIndex: 99})
        this.setState({text: localText})
    }

    // Delete selected text
    deleteText(text) {
        let localText = this.state.text
        let index = localText.indexOf(text)
        localText.splice(index, 1)
        this.setState({text: localText})
    }

    // Delete selected image
    deleteImage(image) {
        let localImage = this.state.customImgs
        let index = localImage.indexOf(image)
        localImage.splice(index, 1)
        this.setState({customImgs: localImage})
    }

    // Hide navbar items
    toggleBar(fixedMode = null) {
        this.componentDidMount()
        if (this.state.hiddenBar.mode === false && typeof fixedMode === 'object') {
            this.setState({hiddenBar: {mode: true, style: {'display': 'none', 'margin': '0px'}}})
        } else if (this.state.hiddenBar.mode === true && typeof fixedMode === 'object') {
            this.setState({hiddenBar: {mode: false, style: {'display': 'block', 'margin': '0px'}}})
        }
        if (this.state.hiddenBar.mode === false && fixedMode === 'view') {
            this.setState({hiddenBar: {mode: true, style: {'display': 'none', 'margin': '0px'}}})
        }
    }

    // Hide input in card
    hideInput() {
        let localIds = this.state.listMultipleDragIds.slice()
        let localCard = this.state.cards.slice()
        localIds.map(id => {
            if (!localCard[id].hasOwnProperty('hideInput')) {
                localCard[id].hideInput = true
            } else {
                localCard[id].hideInput = !localCard[id].hideInput
            }
        })
        this.setState({cards: localCard})
    }

    hideShadow() {
        let localIds = this.state.listMultipleDragIds.slice()
        let localCard = this.state.cards.slice()
        localIds.map(id => {
            if (!localCard[id].hasOwnProperty('hideShadow')) {
                localCard[id].hideShadow = true
            } else {
                localCard[id].hideShadow = !localCard[id].hideShadow
            }
        })
        this.setState({cards: localCard})
    }

    // Show/Hide grid in every page
    toggleGrid() {
        this.setState({showGridPage: !this.state.showGridPage})
    }

    // change selected page
    onChangePage(e, data) {
        this.setState({selectedPage: data.value})
    }

    getYPage() {
        let pageDiv = document.getElementById(`page-${this.state.selectedPage}`).getBoundingClientRect()
        let y = pageDiv.top + (window.pageYOffset || document.documentElement.scrollTop || 0)

        let firstPageDiv = document.getElementById('page-0').getBoundingClientRect()
        let yFirstPage = firstPageDiv.top + (window.pageYOffset || document.documentElement.scrollTop || 0)

        return y - yFirstPage
    }

    // DRAW FUNCTION
    handleEnableDraw(type, options, e) {
        let localDraw = Object.assign({}, this.state.enableDraw)
        if (type !== 'reset') {
            Object.keys(localDraw).forEach(key => key === type ? localDraw[key] = !localDraw[key] : localDraw[key] = false)

            let localDrawOptions = Object.assign({}, this.state.drawOptions)
            localDrawOptions = {...options}
            this.setState({enableDraw: localDraw, drawOptions: localDrawOptions, shapesMode: true})
        } else {
            Object.keys(localDraw).forEach(key => localDraw[key] = false)
            this.setState({enableDraw: localDraw})
        }
    }

    handleMouseDownStage(e) {
        if (Object.values(this.state.enableDraw).filter(item => item === true).length > 0) {
            let localShapes = this.state.shapes.slice()
            let type = this.getTypeDraw()
            let name = this.getNewShapeNameByType(type)

            switch (type) {
                case 'rect':
                    localShapes.push({
                        'name': name,
                        'type': type,
                        'x': e.evt.layerX,
                        'y': e.evt.layerY,
                        'width': 0,
                        'height': 0,
                        'stroke': this.state.drawOptions.strokeColor,
                        'fill': this.state.drawOptions.fillColor,
                        'strokeWidth': this.state.drawOptions.strokeWidth
                    })
                    break
                case 'circle':
                    localShapes.push({
                        'name': name,
                        'type': type,
                        'x': e.evt.layerX,
                        'y': e.evt.layerY,
                        'radius': 0,
                        'stroke': this.state.drawOptions.strokeColor,
                        'fill': this.state.drawOptions.fillColor,
                        'strokeWidth': this.state.drawOptions.strokeWidth
                    })
                    break
                case 'arrow':
                    localShapes.push({
                        'name': name,
                        'type': type,
                        'points':[e.evt.layerX, e.evt.layerY, e.evt.layerX, e.evt.layerY],
                        'color': this.state.drawOptions.color,
                        'pointerLength': this.state.drawOptions.pointerLength,
                        'pointerWidth': this.state.drawOptions.pointerWidth,
                        'strokeWidth': this.state.drawOptions.strokeWidth
                    })
                    break
                case 'line':
                    localShapes.push({
                        'name': name,
                        'type': type,
                        'points':[e.evt.layerX, e.evt.layerY, e.evt.layerX, e.evt.layerY],
                        'color': this.state.drawOptions.color,
                        'strokeWidth': this.state.drawOptions.strokeWidth
                    })
                    break
                default:
                    break
            }

            this.setState({shapes: localShapes, isDrawing: true})
        }
    }

    handleMouseMoveStage(e) {
        if (Object.values(this.state.enableDraw).filter(item => item === true).length > 0 && this.state.isDrawing) {
            let localShapes = this.state.shapes.slice()
            let currShapeIndex = this.state.shapes.length - 1
            let type = this.getTypeDraw()
            switch (type) {
                case 'rect':
                    localShapes[currShapeIndex].width = e.evt.layerX - localShapes[currShapeIndex].x
                    localShapes[currShapeIndex].height = e.evt.layerY - localShapes[currShapeIndex].y
                    break
                case 'circle':
                    let deltaX = localShapes[currShapeIndex].x - e.evt.layerX
                    let deltaY = localShapes[currShapeIndex].y - e.evt.layerY
                    localShapes[currShapeIndex].radius = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2))
                    break
                case 'arrow':
                    localShapes[currShapeIndex].points = [localShapes[currShapeIndex].points[0], localShapes[currShapeIndex].points[1], e.evt.layerX, e.evt.layerY]
                    break
                case 'line':
                    localShapes[currShapeIndex].points = [localShapes[currShapeIndex].points[0], localShapes[currShapeIndex].points[1], e.evt.layerX, e.evt.layerY]
                    break
                default:
                    break
            }

            this.setState({shapes: localShapes})
        }
    }

    handleMouseUpStage(e) {
        if (Object.values(this.state.enableDraw).filter(item => item === true).length > 0 && this.state.isDrawing) {
            this.setState({isDrawing: false}, () => this.minimizaShapes())
        }
    }

    // Delete che current selected shape
    deleteCurrentDraw() {
        if (this.state.selectedShapeName !== '') {
            let localShapes = this.state.shapes
            let index = localShapes.findIndex(item => item.name === this.state.selectedShapeName)
            if (index >= 0) {
                localShapes.splice(index, 1)
                this.setState({shapes: localShapes, selectedShapeName: ''})
            }
        }
    }

    updateShape(shapeName, e) {
        let localShapes = this.state.shapes.slice()
        if (shapeName === this.state.selectedShapeName) {
            let index = localShapes.findIndex(s => s.name === shapeName)
            if (index >= 0) {
                let type = localShapes[index].type
                localShapes[index] = Object.assign({}, e.target.attrs)
                localShapes[index].type = type
                this.setState({shapes: localShapes})
            }
        }
    }

    handleTransform(e) {
        // clicked on stage - cler selection
        if (e.target === e.target.getStage()) {
            this.setState({selectedShapeName: ''})
            return
        }
        // clicked on transformer - do nothing
        const clickedOnTransformer =
        e.target.getParent().className === 'Transformer'
        if (clickedOnTransformer) {
            return
        }
        
        // find clicked shape by its name
        const name = e.target.name()
        // const shape = this.state.shapes.find(s => s.name === name && s.type !== 'arrow')
        const shape = this.state.shapes.find(s => s.name === name)
        if (shape) {
            this.setState({selectedShapeName: name})
        } else {
            this.setState({selectedShapeName: ''})
        }
    }

    getTypeDraw() {
        let type
        Object.keys(this.state.enableDraw).forEach(key => {
            if (this.state.enableDraw[key]) {
                type = key
            }
        })
        return type
    }

    // Handle shapes mode on/off
    handleShapesMode() {
        if (Object.values(this.state.enableDraw).every(v => !v)) {
            this.setState({shapesMode: !this.state.shapesMode,
                selectedShapeName: this.state.selectedShapeName !== '' ? '' : this.state.selectedShapeName})
        }
    }

    // Create the new shape name
    getNewShapeNameByType(type) {
        let localShapes = this.state.shapes.slice()
        let name = `${type}-${localShapes.filter(s => s.type === type).length}`
        return name
    }
    
    // Minimize shapes array
    minimizaShapes() {
        let localShapes = this.state.shapes.slice()
        localShapes = localShapes.filter((item, index) => {
            if (item.type === 'rect' && item.width === 0 && item.height === 0) {
                return false
            }
            if (item.type === 'circle' && item.radius === 0) {
                return false
            }
            if (item.type === 'arrow' && item.points[0] === item.points[2] && item.points[1] === item.points[3]) {
                return false
            }
            if (item.type === 'line' && item.points[0] === item.points[2] && item.points[1] === item.points[3]) {
                return false
            }
            return true
        })
        this.setState({shapes: localShapes})
    }

    // SLEEP (is a test instead of classic setTimeout)
    sleep(time) {
        return new Promise((resolve) => setTimeout(resolve, time))
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        // Vede se si è in view mode e setta lo style per nascondere i pulsanti se serve
        let isView = this.props.match.params.mode === 'edit' ? false : true
        let hideButton = {}
        let enableResizing = {}
        if (isView === true || this.state.shapesMode) {
            if (isView === true) {
                hideButton = {'display': 'none'}
                this.toggleBar('view')
            }
            enableResizing = {
                            bottom: false,
                            bottomLeft: false,
                            bottomRight: false,
                            left: false,
                            right: false,
                            top: false,
                            topLeft: false,
                            topRight: false,
                            }
        } else {
            enableResizing = {
                            bottom: true,
                            bottomLeft: true,
                            bottomRight: true,
                            left: true,
                            right: true,
                            top: true,
                            topLeft: true,
                            topRight: true,
                            }
        }

        if (this.state.savedProject.length === 0) {
            return (
                <Dimmer
                    active={!this.state.finishLoad}
                    page
                >
                    <Loader active inline='centered' size='massive' />
                    <br />
                    <Button color='red' as={Link}
                        to={'/basic/' + this.props.match.params.mode + '/' + this.props.match.params.projectid + '/' + this.props.match.params.chapterid}
                    >
                        {t("HEAD_BTN_RETURN")}
                    </Button>
                </Dimmer>
            )
        } else {
            let customImgsLayout = this.state.customImgs
            customImgsLayout = customImgsLayout.map((img,index) => {
                // Build Src Path for the images
                let tmpProjectId = this.props.match.params.projectid
                let tmpChapterId = this.props.match.params.chapterid
                let fileName
                if (img.old && img.old === true) {
                    fileName = img.name
                } else {
                    fileName = tmpProjectId + '_' + tmpChapterId + '_' + img.name
                }
                let composedSrc = window.env.MediaImage + tmpProjectId + '/' + fileName

                //stampo le immagini custom
                return(
                    <Rnd
                        key={index}
                        style={{background: '#ddd', 'zIndex': img.zIndex}}
                        position={{ x: img.x, y: img.y}}
                        size={{ height: img.height, width: img.width}}
                        onDragStop={this.dragImg.bind(this, img, index)}
                        onResize={this.resizeImg.bind(this, img, index)}
                        disableDragging = {isView || this.state.shapesMode}
                        enableResizing = {enableResizing}
                        className='customImgTypo'
                        onClick={this.selectCustomImg.bind(this, img, 'image')}
                    >
                        <Image id={'dndImg-' + index} style={{'height': '100%'}} className='imgFullWidth' src={composedSrc}/>
                    </Rnd>
                )
            })

            let cardsLayout = this.state.cards
            cardsLayout = cardsLayout.map((item, index) => {
                return (
                    <Rnd
                        key={index}
                        position={{ x: item.x, y: item.y }}
                        size={{width: item.rndWidth, height: item.rndHeight}}
                        onDragStop={this.drag.bind(this, item)}
                        onResize={this.resize.bind(this, item)}
                        disableDragging = {isView || this.state.shapesMode}
                        enableResizing = {enableResizing}
                        minHeight= '45'
                        minWidth= '30'
                        className={'typoCard-' + index}
                        style={{zIndex: '99'}}
                    >
                        <div style={{"width": "100%", "height":"100%"}} onClick={this.checkMultipleDrag.bind(this, item)}>
                            <CardLayout
                                Card={item}
                                isTypo={true}
                                imgFullWidth={false}
                                mode={true}
                                posInput= {this.state.profile.posInput}
                                sizeInput= {this.state.profile.sizeInput}
                                styleInput= {this.state.profile.styleInput}
                                formatInput= {this.state.profile.formatInput}
                                colorTextInput= {this.state.profile.colorTextInput}
                                colorBackgroundInput= {this.state.profile.colorBackgroundInput}
                                weightInput= {this.state.profile.weightInput}
                                decorationInput= {this.state.profile.decorationInput}
                                fontStyleInput= {this.state.profile.fontStyleInput}
                                imgSize= {this.state.customSymbolSize}
                                imgPadding= {this.state.profile.imgPadding}
                                imgType= {this.state.profile.imgType}
                                borderCard= {this.state.profile.borderCard}
                                watermark={this.props.watermark}
                            />
                        </div>
                    </Rnd>
                )
            })

            // render custom text
            let text = this.state.text
            text = text.map((item, index) => {
                return (
                    <Rnd
                        key={index}
                        position={{ x: item.x, y: item.y }}
                        onDragStop={this.dragText.bind(this, item, index)}
                        onResize={this.resizeText.bind(this, item, index)}
                        disableDragging = {isView || this.state.shapesMode}
                        enableResizing = {enableResizing}
                        style={{border: item.border, padding: '3px', zIndex: item.zIndex}}
                        onClick={this.selectCustomImg.bind(this, item, 'text')}
                    >
                        <Segment id={'customText-' + index} size={item.size}
                            color={item.color}
                            inverted={item.inverted}
                            style={{'height': '100%'}}

                        >
                            <div dangerouslySetInnerHTML={{__html: item.text}} />
                        </Segment>
                    </Rnd>
                )
            })

            // Render the shapes
            let localShapes = this.state.shapes.slice()
            localShapes = localShapes.map((item, index) => {
                switch (item.type) {
                    case 'rect':
                        return (
                            <Rect
                                key={index}
                                draggable
                                x={item.x}
                                y={item.y}
                                name={item.name}
                                width={item.width}
                                height={item.height}
                                stroke={item.stroke}
                                strokeWidth={item.strokeWidth}
                                fill={item.fill}
                                rotation={item.rotation}
                                scaleX={item.scaleX}
                                scaleY={item.scaleY}
                                onTransformEnd={this.updateShape.bind(this, item.name)}
                                onDragEnd={this.updateShape.bind(this, item.name)}

                            />
                        )
                        break
                    case 'circle':
                        return (
                            <Circle
                                key={index}
                                draggable
                                x={item.x}
                                y={item.y}
                                radius={item.radius}
                                name={item.name}
                                stroke={item.stroke}
                                strokeWidth={item.strokeWidth}
                                fill={item.fill}
                                rotation={item.rotation}
                                scaleX={item.scaleX}
                                scaleY={item.scaleY}
                                onTransformEnd={this.updateShape.bind(this, item.name)}
                                onDragEnd={this.updateShape.bind(this, item.name)}
                            />
                        )
                        break
                    case 'arrow':
                        return (
                            <Arrow
                                key={index}
                                draggable
                                x={item.x}
                                y={item.y}
                                points={item.points}
                                name={item.name}
                                stroke={item.color || item.stroke}
                                fill={item.color || item.fill}
                                tension={1}
                                pointerLength={item.pointerLength}
                                pointerWidth={item.pointerWidth}
                                strokeWidth={item.strokeWidth}
                                onTransformEnd={this.updateShape.bind(this, item.name)}
                                onDragEnd={this.updateShape.bind(this, item.name)}
                            />
                        )
                        break
                    case 'line':
                        return (
                            <Line
                                key={index}
                                draggable
                                x={item.x}
                                y={item.y}
                                points={item.points}
                                name={item.name}
                                stroke={item.color || item.stroke}
                                fill={item.color || item.fill}
                                strokeWidth={item.strokeWidth}
                                rotation={item.rotation}
                                scaleX={item.scaleX}
                                scaleY={item.scaleY}
                                onTransformEnd={this.updateShape.bind(this, item.name)}
                                onDragEnd={this.updateShape.bind(this, item.name)}
                            />
                        )
                        break
                    default:
                        break
                }

            })

            // render the divs page
            let segmentPage = Object.assign([], this.state.pagesNumber)
            segmentPage = segmentPage.map((item, index) => {
                let gridPage = this.state.showGridPage ? 'gridTypoDocument' : ''
                let cardPerPage = []
                for (let i = 0; i < this.state.cards.length; i++) {
                    if (this.state.cards[i].page === index) {
                        cardPerPage.push(cardsLayout[i])
                    }
                }
                if (index === 0) {
                    return(
                        <div key={index}>
                            <Segment className={`${this.state.classSheet} ${gridPage} section-to-print`}
                                id={`page-${index}`}
                                style={{'width': this.state.layout.width, 'height': this.state.layout.height}}
                                onClick={this.selectCustomImg.bind(this, null, 'page')}
                            >
                                {text}
                                {customImgsLayout}
                                {cardPerPage}
                            </Segment>
                            <Divider className='no-print' fitted />
                            {/* <br className='no-print' /> */}
                        </div>
                    )
                } else {
                    return(
                        <div key={index}>
                            <Divider className='no-print' fitted />
                            <Label className='position-absolute no-print'>{t("TYPO_LBL_PAGE")} - {index + 1}</Label>
                            <Segment className={`${this.state.classSheet} ${gridPage} section-to-print`}
                                id={`page-${index}`}
                                style={{'width': this.state.layout.width, 'height': this.state.layout.height}}
                                onClick={this.selectCustomImg.bind(this, null, 'page')}
                            >
                                {cardPerPage}
                            </Segment>
                            {/* <br className='no-print' /> */}
                        </div>
                    )
                }
            })

            let pageDropdownOptions = this.state.pagesNumber.slice()
            pageDropdownOptions = pageDropdownOptions.map(item => {
                return {text: `page: ${item + 1}`, value: item}
            })

            // Main render return
            return (
                <div onKeyDown={this.setMultipleDrag.bind(this)} onKeyUp={this.setMultipleDrag.bind(this)} tabIndex="0">
                    <Segment.Group className='no-print position-fixed' id='typo-menu' style={{width: '100%', zIndex: '999'}}>
                        <Segment className='fix-display'>
                            <Button color='green'
                                disabled={this.state.loadingButton}
                                loading={this.state.loadingButton}
                                onClick={this.saveTypo.bind(this)}
                                style={hideButton}
                            >
                                {t("HEAD_BTN_SAVE")}
                            </Button>
                            {
                            // <Button color='blue' onClick={this.printDocument}>{t("HEAD_BTN_EXPORTPDF")}</Button>
                            }
                            <Button color='blue'
                                disabled={isView}
                                style={hideButton}
                                onClick={this.toggleBar.bind(this)}
                            >
                                {this.state.hiddenBar.mode === false ? 'View' : 'Edit'}
                            </Button>
                            <Button color='blue' onClick={() => {window.print()}}>{t("HEAD_BTN_PRINT")}</Button>
                            <Button color='red' as={Link}
                                to={'/basic/' + this.props.match.params.mode + '/' + this.props.match.params.projectid + '/' + this.props.match.params.chapterid}
                            >
                                {t("HEAD_BTN_RETURN")}
                            </Button>
                        </Segment>
                        <Menu size='large' style={this.state.hiddenBar.style}>
                            <Menu.Menu>
                                <Dropdown placeholder={t("TYPO_FRM_PLACEHOLDER")} selection
                                    disabled={this.state.hasTypoSaved}
                                    style={{'zIndex': 100}}
                                    options={this.state.imageSize}
                                    onChange={this.onChangeDropdown.bind(this)}
                                />
                                <Dropdown item text={t("TYPO_ADDITEMS")} style={{'zIndex': 100}}>
                                    <Dropdown.Menu>
                                        <AddCustomText
                                            disabled={isView}
                                            style={hideButton}
                                            addText={this.addText.bind(this)}
                                        />
                                        <Can perform='can_upload_media'>
                                            <CustomImgsDnDUpload
                                                disabled={isView}
                                                style={hideButton}
                                                handler={this.handler}
                                                className='dropdown-item-hover icon-pointer'
                                            />
                                        </Can>
                                        <Dropdown.Item
                                            disabled={isView}
                                            style={hideButton}
                                            onClick={this.addPage.bind(this)}
                                        >
                                            {t("TYPO_BTN_ADDPAGE")}
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            disabled={isView}
                                            style={hideButton}
                                            onClick={this.hideInput.bind(this)}
                                        >
                                            {t("TYPO_BTN_HIDEINPUT")}
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            disabled={isView}
                                            style={hideButton}
                                            onClick={this.hideShadow.bind(this)}
                                        >
                                            {t("TYPO_BTN_HIDESHADOW")}
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            disabled={isView}
                                            style={hideButton}
                                            onClick={this.toggleGrid.bind(this)}
                                        >
                                            {this.state.showGridPage ? t("TYPO_BTN_HIDEGRID") : t("TYPO_BTN_SHOWGRID")}
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                <Dropdown item text={t("TYPO_RESET")} style={{'zIndex': 100}} simple>
                                    <Dropdown.Menu>
                                        <Dropdown.Item
                                            disabled={isView}
                                            style={hideButton}
                                            onClick={this.resetTypo.bind(this)}
                                        >
                                            {t("HEAD_BTN_RESET")}
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            disabled={isView}
                                            style={hideButton}
                                            onClick={this.resetTypo.bind(this, 'full')}
                                        >
                                            {t("TYPO_RESETFULL")}
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                <Draw className='icon-pointer'
                                    draw={this.handleEnableDraw.bind(this)}
                                />
                                <Menu.Item
                                    onClick={this.handleShapesMode.bind(this)}
                                    style={this.state.shapesMode === true ? {backgroundColor: 'darkred', color: 'white'} : {color: 'black'}}
                                >
                                    <div>
                                        {t("TYPO_BTN_SHAPESMODE")}
                                    </div>
                                </Menu.Item>
                                <Dropdown item text={t("TYPO_DELETEITEMS")} style={{'zIndex': 100}} simple>
                                    <Dropdown.Menu>
                                        <Dropdown.Item>
                                             <DeleteCustomElement
                                                disabled={isView}
                                                style={hideButton}
                                                text={this.state.text}
                                                image={this.state.customImgs}
                                                projectid ={this.props.match.params.projectid}
                                                chapterid ={this.props.match.params.chapterid}
                                                deleteText={this.deleteText.bind(this)}
                                                deleteImage={this.deleteImage.bind(this)}
                                                className='icon-pointer'
                                            />
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={this.deleteCurrentDraw.bind(this)}
                                        >
                                            {t("TYPO_BTN_DELETESHAPE")}
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={this.modifyZindexElements.bind(this, 'up')}
                                        >
                                            {t("TYPO_BTN_ZINDEXUP")}
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={this.modifyZindexElements.bind(this, 'down')}
                                        >
                                            {t("TYPO_BTN_ZINDEXDOWN")}
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                <Dropdown placeholder={t("TYPO_FRM_PLACEHOLDER")} selection simple
                                    style={{'zIndex': 100}}
                                    options={pageDropdownOptions}
                                    value={this.state.selectedPage}
                                    onChange={this.onChangePage.bind(this)}
                                />
                            </Menu.Menu>
                        </Menu>
                    </Segment.Group>
                    <Message
                        positive
                        compact={true}
                        hidden={this.state.saveMessage}
                        icon={'save'}
                        header='Projet Saved'
                    />
                    <Stage width={this.state.layout.width} height={this.state.drawCanvasHeight}
                        onContentMouseDown={this.handleMouseDownStage.bind(this)}
                        onContentMouseMove={this.handleMouseMoveStage.bind(this)}
                        onContentMouseUp={this.handleMouseUpStage.bind(this)}
                        onMouseDown={this.handleTransform.bind(this)}
                        style={!this.state.shapesMode ? {pointerEvents: 'none'} : {pointerEvents: 'all'}}
                    >
                        <Layer>
                            {localShapes}
                            <TransformerDraw selectedShapeName={this.state.selectedShapeName} />
                        </Layer>
                    </Stage>
                    <div id='pageWrapper'
                        style={{top: `${this.state.menuHeight + 20}px`, position: 'absolute'}}
                    >
                        {segmentPage}
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        chapterContent: state.chapterContentData,
        chapterRows: state.chapterRowsData,
        typoContent: state.chapterTypoData,
        projects: state.projectData,
        projectID: state.selectedProjectID,
        dropdownImgSize: state.selectedImgSize,
        watermark: state.watermarkData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        chapterToTypo: (content) => {dispatch(chapterTypoDataFetchDataSuccess(content))},
        setTypoDropdown: (size) => {dispatch(dispatchTypoImgSize(size))},
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(withApolloFetch(withRouter(LayoutExport))))