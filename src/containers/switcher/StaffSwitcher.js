import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import { connect } from 'react-redux'
import { apolloFetchNoAuth } from '../../store/apolloFetchUtils'
import { saveTeams } from "../../store/actions/AdministrationActions";
import { userFetchDataSuccess } from "../../store/actions/UserActions";

class StaffSwitcher extends Component {
    constructor(props) {
        super(props)
        this.state = {
            staffs: []
        }
    }
    
    componentDidMount() {
        let query = `
            query fetchStaffs {
                staffs(limit: 100) {
                    data {
                        id
                        name
                    }
                }
            }
        `
        apolloFetchNoAuth({query})
            .then(data => {
                if (this.props.sort) {
                    let sortedStaffs = data.data.staffs.data.sort((a, b) => {
                      var keyA = a[this.props.sort].toUpperCase();
                      var keyB = b[this.props.sort].toUpperCase();
                      if (keyA < keyB) {
                        return -1;
                      }
                      if (keyA > keyB) {
                        return 1;
                      }
                      return 0;
                    });
                    this.setState({staffs: sortedStaffs});
                } else {
                  this.setState({ staffs: data.data.staffs.data });
                }
            })
            .catch(error => console.error(error))
    }
    
    // Handle change staff
    changeStaff(idstaff, e) {
        this.props.refreshTeam(null, idstaff)

        let localUser = Object.assign({}, this.props.user)
        localUser.idstaff = idstaff
        this.props.updateUser(localUser)
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        let options = this.state.staffs.map((item, index) => {
            return (
                <Dropdown.Item key={index} onClick={this.changeStaff.bind(this, item.id)}>{item.name}</Dropdown.Item>
            )
        })

        return (
            <Dropdown item text={t("PRLD_DROP_STAFF")}>
                <Dropdown.Menu>
                    {options}
                </Dropdown.Menu>
            </Dropdown>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        refreshTeam: (user_id, staff_id) => dispatch(saveTeams(user_id, staff_id)),
        updateUser: (user) => dispatch(userFetchDataSuccess(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(StaffSwitcher))
