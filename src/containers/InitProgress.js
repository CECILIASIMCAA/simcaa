import React, { Component, Fragment } from 'react'
import { withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'

import InitProgressBar from '../components/InitProgressBar'
import { projectDataFetchData } from '../store/actions/ProjectFetchActions'
import { profileDataFetchData } from '../store/actions/ProfileFetchActions'
import { layoutDataFetchData } from '../store/actions/LayoutFetchActions'
import { classDataFetchData } from '../store/actions/ClassFetchActions'
import { styleDataFetchData } from '../store/actions/StyleFetchActions'
import { symstyleDataFetchData } from '../store/actions/SymStyleFetchActions'
import { userTeamDataFetchData } from '../store/actions/UserTeamFetchActions'
import { watermarkDataFetchData } from '../store/actions/WatermarkFetchActions'
import { userToHome } from '../store/actions/UserActions'
import { filterSetValue } from '../store/actions/FilterActions'
import { membersTeamDataFetchData } from '../store/actions/UserTeamFetchActions'
import { Modal, Button, Form, Label, Message, Checkbox, Segment } from 'semantic-ui-react'

import { apolloFetch } from '../store/apolloFetchUtils'

class InitProgress extends Component {
    constructor(props) {
        super(props)
        this.state= {
            newPassword: '',
            repeatPassword: '',
            indexModal: 0,
            policyChecked: false,
            privacyChecked: false,
            error: {hidden: true, header: 'Errore', content: 'test'},
            successMessage: true,
            limit: 15,
            page: 1,
            percent: 12,
            label: 'Loading Data',
            profileCalled: false,
            teamCalled: false,
            listProgress: [{iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_PROJECT")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_PROFILE")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_CLASS")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_LAYOUT")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_STYLE")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_SYMSTYLE")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_TEAM")},
                        {iconName: 'spinner', loading: true, color: 'black', innerString: props.t("INIT_FETCH_WATERMARK")}]
        }
    }

    componentDidMount() {
        if (this.props.user && this.props.user.last_login) {
            this.props.userToHome(false)
            this.props.fetchUserTeam()
            this.props.fetchProject()
            this.props.fetchLayout()
            this.props.fetchClass()
            this.props.fetchStyle()
            this.props.fetchSymStyle()
            this.props.fetchWatermark()
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.user && this.props.user.last_login) {
            // push to home page
            if (this.props.user.length !== 0 && this.state.listProgress.every(x => x.loading === false) && prevProps === this.props) {
                if (this.props.team.length > 1) {
                    setTimeout(() => {
                        this.props.history.push('/teamselect')
                    }, 1000)
                } else {
                    setTimeout(() => {
                        this.props.history.push('/home')
                    }, 1000)
                }
            }
    
            if (this.props.userTeamCompleted && !this.state.teamCalled) {
                let indexTeam = this.props.team.findIndex(x => x.user_id === this.props.user.id)
                if(indexTeam > -1){
                    this.props.changeFilter(this.props.team[indexTeam].team_id)
                }
                this.props.memberTeam()
                this.setState({teamCalled: true })
            }
    
            // check the progress bar
            let localListProgress = this.state.listProgress.slice()
    
            this.checkFetchStatus(this.props.projectLoading, this.props.projectError, localListProgress, 0)
            this.checkFetchStatus(this.props.profileLoading, this.props.profileError, localListProgress, 1)
            this.checkFetchStatus(this.props.layoutLoading, this.props.layoutError, localListProgress, 2)
            this.checkFetchStatus(this.props.classLoading, this.props.classError, localListProgress, 3)
            this.checkFetchStatus(this.props.styleLoading, this.props.styleError, localListProgress, 4)
            this.checkFetchStatus(this.props.symstyleLoading, this.props.symstyleError, localListProgress, 5)
            this.checkFetchStatus(this.props.userTeamLoading, this.props.userTeamError, localListProgress, 6)
            this.checkFetchStatus(this.props.watermarkLoading, this.props.watermarkError, localListProgress, 7)
    
            // Fetch profile after getting the team
            if (this.props.userTeamCompleted && !this.state.profileCalled) {
                this.props.fetchProfile()
                this.setState({profileCalled: true })
            }
        }
    }

    // if no error call the update to the progress bar
    checkFetchStatus(loading, error, data, index) {
        if (!loading && !error && this.state.percent < 100) {
            if (data[index].loading) {
                this.updateProgressBar(data, index, 11)
            }
        }
    }

    // Update the progress bar state
    updateProgressBar(data, index, progressValue) {
        data[index].iconName = 'check'
        data[index].loading = false
        data[index].color = 'green'
        this.setState({percent: this.state.percent + progressValue, listProgress: data})
    }

    // handle change input
    handleChangePassword(password, e) {
        this.setState({[password]: e.target.value})
    }

    // Change password
    handleUpdatePassword() {
        let localError = Object.assign({}, this.state.error)
        if (this.state.newPassword !== '' && this.state.repeatPassword !== '') {
            if (this.state.newPassword === this.state.repeatPassword) {
                let query = `
                    mutation UpdateUser {
                        updateCaaUser(id: ${this.props.user.id}, last_login: ${true}, new_password: "${this.state.newPassword}"){
                            id
                        }
                    }
                `
                apolloFetch({query})
                    .then(data => {
                        if (data.data.updateCaaUser.id !== -1) {
                            // PASSWORD CHANGED
                            localError.hidden = true
                            this.setState({error: localError, successMessage: !this.state.successMessage}, () => {
                                setTimeout(() => {
                                    this.props.history.push('/login')
                                }, 1000)
                            })
                        } else {
                            // PASSWORD ERROR
                            localError.content = this.props.t("PWD_APIERROR")
                            localError.hidden = false
                            this.setState({error: localError, successMessage: true})
                        }
                    })
                    .catch(error => console.log(error))
            } else {
                // ERROR in data
                localError.content = this.props.t("PWD_MISMATCH")
                localError.hidden = false
                this.setState({error: localError, successMessage: true})
            }
        } else {
            // ERROR in data
            localError.content = this.props.t("PWD_FILLFORM")
            localError.hidden = false
            this.setState({error: localError, successMessage: true})
        }
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        if (!this.props.user.last_login) {
            let labelPassword
            if (this.state.newPassword !== '' || this.state.repeatPassword !== '') {
              if (this.state.newPassword === this.state.repeatPassword) {
                labelPassword = <Label color='green' pointing='left'>{t("PWD_FORMCORRECT")}</Label>          
              } else {
                labelPassword = <Label color='red' pointing='left'>{t("PWD_FORMERROR")}</Label>
              }
            }

            let modalContent = [],
                modalAction = []

            modalContent[0] = (
                <Fragment>
                  <Modal.Content>
                    <Segment style={{overflow: 'auto', maxHeight: 250 }} color='blue'>
                      <div dangerouslySetInnerHTML={{ __html: window.login_document.policy }} />
                    </Segment>
                    <Segment style={{overflow: 'auto', maxHeight: 250 }} color='green'>
                      <div dangerouslySetInnerHTML={{ __html: window.homepage_document.gdpr }} />
                    </Segment>
                    </Modal.Content>
                </Fragment>
            );
            modalContent[1] = (
              <Modal.Content>
                <Form>
                  <Form.Field width={16}>
                    <label>{t("MAIN_PROFILE_PWDNEW")}</label>
                    <input
                      placeholder={t("MAIN_PROFILE_PWDNEW")}
                      type="password"
                      style={{ width: "50%" }}
                      value={this.state.newPassword}
                      onChange={this.handleChangePassword.bind(
                        this,
                        "newPassword"
                      )}
                    />
                    {labelPassword}
                  </Form.Field>
                  <Form.Field width={16}>
                    <label>{t("MAIN_PROFILE_PWDREPEAT")}</label>
                    <input
                      placeholder={t("MAIN_PROFILE_PWDREPEAT")}
                      type="password"
                      style={{ width: "50%" }}
                      value={this.state.repeatPassword}
                      onChange={this.handleChangePassword.bind(
                        this,
                        "repeatPassword"
                      )}
                    />
                    {labelPassword}
                  </Form.Field>
                </Form>
                <Message
                  hidden={this.state.error.hidden}
                  negative
                  icon="exclamation circle"
                  header={this.state.error.header}
                  content={this.state.error.content}
                />
                <Message
                  hidden={this.state.successMessage}
                  positive
                  icon="check"
                  header={"Successo!!!"}
                  content={t("PWD_CHANGECORRECT")}
                />
              </Modal.Content>
            );

            modalAction[0] = (
                <Fragment>
                  <Button negative as={Link} to="/login">
                    {t("HOME_NAVBAR_USER_LOGOUT")}
                  </Button>
                  <Button
                    primary 
                    onClick={() => this.setState({indexModal: 1})}
                    disabled={!this.state.policyChecked || !this.state.privacyChecked}
                  >
                    {t("MAIN_BTN_FORWARD")}
                  </Button>
                </Fragment>
              );
            modalAction[1] = (
              <Fragment>
                <Button negative as={Link} to="/login">
                  {t("HOME_NAVBAR_USER_LOGOUT")}
                </Button>
                <Button color='green' onClick={() => this.setState({indexModal: 0})}>
                  {t("MAIN_BTN_BACK")}
                </Button>
                <Button primary onClick={this.handleUpdatePassword.bind(this)}>
                  {t("BTN_CONFIRM")}
                </Button>
              </Fragment>
            );

            return (
                <Modal defaultOpen={true} closeOnDimmerClick={false}>
                    <Modal.Header>{t("HDR_FIRSTACCESS")}</Modal.Header>
                        {modalContent[this.state.indexModal]}
                        {
                          this.state.indexModal === 0 ?
                          <Modal.Content>
                            <Segment.Group>
                              <Segment basic textAlign="center">
                                <Checkbox
                                  label={t("INIT_POLICY_ACCEPT_LBL")}
                                  checked={this.state.policyChecked}
                                  onChange={() =>
                                    this.setState({
                                      policyChecked: !this.state.policyChecked
                                    })
                                  }
                                />
                              </Segment>
                              <Segment basic textAlign="center">
                                <Checkbox
                                  label={t("INIT_PRIVACY_ACCEPT_LBL")}
                                  checked={this.state.privacyChecked}
                                  onChange={() =>
                                    this.setState({
                                      privacyChecked: !this.state.privacyChecked
                                    })
                                  }
                                />
                              </Segment>
                            </Segment.Group>
                          </Modal.Content>
                          : null
                        }
                    <Modal.Actions>
                        {modalAction[this.state.indexModal]}
                    </Modal.Actions>
                </Modal>
            )
        }
        return (
            <InitProgressBar
                percent={this.state.percent}
                label={this.state.label}
                listProgress={this.state.listProgress}
                list
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        projectError: state.projectDataHasErrored,
        projectLoading: state.projectDataIsLoading,
        profileError: state.profileDataHasErrored,
        profileLoading: state.profileDataIsLoading,
        layoutError: state.layoutDataHasErrored,
        layoutLoading: state.layoutDataIsLoading,
        classError: state.classDataHasErrored,
        classLoading: state.classDataIsLoading,
        styleError: state.styleDataHasErrored,
        styleLoading: state.styleDataIsLoading,
        symstyleError: state.symstyleDataHasErrored,
        symstyleLoading: state.symstyleDataIsLoading,
        team: state.userTeamData,
        userTeamError: state.userTeamDataHasErrored,
        userTeamLoading: state.userTeamDataIsLoading,
        userTeamCompleted: state.userTeamDataIsCompleted,
        watermarkError: state.watermarkDataHasErrored,
        watermarkLoading: state.watermarkDataIsLoading,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userToHome: (value) => dispatch(userToHome(value)),
        fetchProject: (query, limit, page) => dispatch(projectDataFetchData(query, limit, page)),
        fetchProfile: (query) => dispatch(profileDataFetchData(query)),
        fetchLayout: (query) => dispatch(layoutDataFetchData(query)),
        fetchClass: (query) => dispatch(classDataFetchData(query)),
        fetchStyle: (query) => dispatch(styleDataFetchData(query)),
        fetchSymStyle: (query) => dispatch(symstyleDataFetchData(query)),
        fetchUserTeam: (query) => dispatch(userTeamDataFetchData(query)),
        fetchWatermark: (query) => dispatch(watermarkDataFetchData(query)),
        changeFilter: (value) => dispatch(filterSetValue(value)),
        memberTeam: (query) => dispatch(membersTeamDataFetchData(query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(translate('translations')(InitProgress)))
