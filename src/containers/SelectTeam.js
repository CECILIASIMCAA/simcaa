import React, { Component } from 'react'
import { Segment, Grid, Button, Message, Header, Icon } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import { translate } from 'react-i18next'

import { connect } from 'react-redux'
import { filterSetValue } from '../store/actions/FilterActions'
import TeamSwitcher from './switcher/TeamSwitcher'

class SelectTeam extends Component {
    constructor(props) {
        super(props)
        this.state= {
            errorMessage: {hidden: true, text: props.t("TMSEL_MSG_ERROR")}
        }
    }

    componentDidMount() {
        this.props.changeFilter(0)
    }

    handleContinue() {
        if (this.props.filterValue === 0) {
            this.setState({errorMessage: {hidden: false, text: this.props.t("TMSEL_MSG_ERROR")}})
        } else {
            this.props.history.push('/home')
        }
    }

    render() {
        const { t } = this.props

        return (
            <Grid
                textAlign='center'
                style={{ height: '100vh', margin: 'auto' }}
                verticalAlign='middle'
                columns={3}
            >
                <Grid.Column>
                    <Segment>
                        <Message
                            color='red'
                            hidden={this.state.errorMessage.hidden}
                            header='Error'
                            content={this.state.errorMessage.text}
                        />
                        <Header as='h5'>
                            {t("TMSEL_HEADER")}
                        </Header>
                        <TeamSwitcher />
                        <Segment basic>
                            <Button icon labelPosition='right' primary onClick={this.handleContinue.bind(this)}>
                                {t("BTN_CONTINUE")}
                                <Icon name='right arrow' />
                            </Button>
                        </Segment>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        filterValue: state.filterValue,
        team: state.userTeamData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeFilter: (value) => dispatch(filterSetValue(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(translate('translations')(SelectTeam)))
