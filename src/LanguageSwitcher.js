import React, { Component } from 'react'
import { Button, Segment, Dropdown, Flag } from 'semantic-ui-react'
import { translate } from 'react-i18next'

class LanguageSwitcher extends Component {
    render() {
        const { t, i18n } = this.props

        const changeLanguage = (lng) => {
            i18n.changeLanguage(lng);
        }

        if (this.props.type === 'dropdown') {
            return (
                <Dropdown item text={t("HOME_NAVBAR_LANGUAGE")}>
                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => changeLanguage('it')}><Flag name='it' />Italiano</Dropdown.Item>
                        <Dropdown.Item onClick={() => changeLanguage('en')}><Flag name='us' />English</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            )
        }

        return (
            <Segment basic>
                <Button onClick={() => changeLanguage('it')}><Flag name='it' />Italiano</Button>
                <Button onClick={() => changeLanguage('en')}><Flag name='us' />English</Button>
            </Segment>
        )
    }
}

export default translate('translations')(LanguageSwitcher)
