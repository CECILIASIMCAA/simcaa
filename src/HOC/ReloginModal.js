import React, { Component } from 'react'
import { Button, Modal } from 'semantic-ui-react'
import { Redirect, withRouter } from 'react-router-dom'

import { connect } from 'react-redux'
import { compose } from 'redux'
import { jwtFetchData, jwtExpiredOrInvalid } from '../store/actions/JWTActions'
import { userHasErrored } from '../store/actions/UserActions'

import LoginForm from '../components/LoginForm'

const ReloginModal = (WrappedComponent) => {
    return class ReloginModal extends Component {
        constructor(props) {
            super(props)
            this.state = {
                toLogin: false,
            }
        }

        componentDidUpdate(prevProps) {
            if (prevProps.ReloginUser.id !== this.props.ReloginUser.id) {
                if (Object.entries(this.props.userReal).length !== 0 && prevProps.ReloginUser.id === this.props.userReal.id) {
                    this.props.history.push('/init')
                } else {
                    this.toLogin()
                }
            }
        }

        toLogin() {
            sessionStorage.removeItem('state')
            this.props.resetUserError(false)
            this.setState({toLogin: true})
        }

        relogin(user, password) {
            this.props.fetchJWT(window.env.GraphQLLogin, user, password, false)
        }

        render() {
            if (this.props.ReloginUser.status === 1 || this.state.toLogin === true || (!sessionStorage.getItem('jwt') && !sessionStorage.getItem('state'))) {
                return (
                    <Redirect to="/login"/>
                )
            } else {
                const { jwtExpiredOrInvalid, jwtHasErrored, jwtIsLoading, ReloginUser, fetchJWT, openModal, resetUserError, ...originalProps} = this.props
                const message = { text: 'Fill the form to relogin into the App', view: this.props.jwtHasErrored, header: 'Welcome back to SimCAA'}
                return (
                    <div>
                        <Modal open={this.props.jwtExpiredOrInvalid}>
                            <Modal.Header>
                                Session time out
                            </Modal.Header>
                            <Modal.Content>
                                <LoginForm
                                    login={this.relogin.bind(this)}
                                    message={message}
                                    loading={this.props.jwtIsLoading}
                                    noMessage
                                />
                            </Modal.Content>
                            <Modal.Actions>
                                <Button negative onClick={this.toLogin.bind(this)}>Exit the app</Button>
                            </Modal.Actions>
                        </Modal>
                        <WrappedComponent {...originalProps} />
                    </div>
                )
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        jwtExpiredOrInvalid: state.jwtExpiredOrInvalid,
        jwtHasErrored: state.jwtHasErrored,
        jwtIsLoading: state.jwtIsLoading,
        ReloginUser: state.user,
        userReal: state.userReal,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchJWT: (url, user, password, reLogin) => dispatch(jwtFetchData(url, user, password, reLogin)),
        openModal: (value) => dispatch(jwtExpiredOrInvalid(value)),
        resetUserError: (value) => dispatch(userHasErrored(value)),
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withRouter,
    ReloginModal
)
