import React, { Component } from 'react'
import { Route, Redirect, withRouter } from 'react-router-dom'

import RootComponent from './containers/homepage/RootComponent'
import BasicProject from './containers/BasicProject'
import LayoutExport from './containers/typo/LayoutExport'
import Project from './containers/project/Project'
import Admin from './administration/Administration'
import Login from './containers/Login'
import InitProgress from './containers/InitProgress'
import SelectTeam from './containers/SelectTeam'
import Preload from './containers/preload/Preload'

import ReloginModal from './HOC/ReloginModal'

// Import CSS files
import './css/grid_styles.css'
import './css/resizable_styles.css'
import './css/react-draft-wysiwyg.css'

class App extends Component {
    componentDidMount() {
		this.props.history.listen(location => {
			sessionStorage.setItem('history', location.pathname)
		})
    }
    
    render() {
        return (
            <div className='full-height'>
                <Route exact path="/" render={() => (<Redirect to="/home" />)}/>
                <Route path="/home" component={ReloginModal(RootComponent)}/>
                <Route path="/project/:projectid?" component={ReloginModal(Project)} />
                <Route path="/basic/:mode?/:projectid?/:chapterid?" component={ReloginModal(BasicProject)} />
                <Route path="/layout/:mode?/:projectid?/:chapterid?" component={ReloginModal(LayoutExport)} />
                <Route path="/login" render={() => (<Login />)}/>
                <Route path="/admin" component={ReloginModal(Admin)} />
                <Route path="/init" component={ReloginModal(InitProgress)} />
                <Route path="/teamselect" component={ReloginModal(SelectTeam)} />
                <Route path="/preload" component={ReloginModal(Preload)} />
            </div>
        )
    }
}

export default withRouter(App)
