import { jwtFetchDataSuccess } from './actions/JWTActions'
import store from './configureStore'

// Escape quotes from string
export function escapeQuotes(item) {
    if (typeof item === 'string' || item instanceof String) {
        return item.replace(/\\([\s\S])|(")/g,"\\$1$2")
    } else {
        return JSON.stringify(item).replace(/\\([\s\S])|(")/g,"\\$1$2")
    }
}

// Test if the item is a valid object
export function isValidObject(item) {
    if (item !== null && typeof item === 'object' && item instanceof Object) {
        return true
    } else {
        return false
    }
}

// Test if object JSON is empty
export function isJsonEmpty(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object
}

// Test if the item is a valid json format
export function isValidJSON(item){
    if(typeof item !== "string") {
        return false
    }
    try {
        JSON.parse(item)
        return true
    }
    catch (error) {
        return false
    }
}

// Reorder ids of an array
export function reorderIds(array, property = 'id') {
    return array.map((item, index) => {
      item[property] = index;
      return item;
    });
}

// Set forus on the input card selected
export function setFocus(index) {
    let inputID = "text-" + index
    setTimeout(() => {
        if (document.getElementById(inputID)) {
            document.getElementById(inputID).focus()
        }
    }, 50)
}

// Auto expand input text width for the current card
export function handleExpandInput(id) {
    let textId = 'text-' + id
    let inputResize = document.getElementById(textId)
    if (inputResize) {
        inputResize.style.width = '0px'
        if (inputResize.scrollWidth > inputResize.clientWidth) {
            inputResize.style.width = inputResize.scrollWidth + 20 + 'px'
        }
    }
}

// Auto expand input text width for all the cards
export function setExpandAll(cards) {
    for (let i = 0; i < cards.length; i++) {
        setTimeout(function() {
            handleExpandInput(cards[i].id)
        }, 0)
    }
}

// Check se il record ha l'immagine custom o no
export function checkWithImage(data) {
    if (data.with_image !== undefined && data.with_image === 1) {
        return true
    } else {
        return false
    }
}

// Set line spacing between cards
export function setLineSpacing(margins) {
    if (margins !== undefined && margins !== null) {
        // TODO: RIVEDERE PER TOGLIERE IL TIMEOUT QUANDO SI AGGIUNGE/ELIMINA UNA CARD
        setTimeout(() => {
            // set linespacing for every cards
            let style = document.getElementsByClassName("cardUI")
            let updateVarMargins = margins.line_spacing_top/2 + 'px ' + margins.line_spacing_left/2 + 'px '
            Object.values(style).map(item => {
                item.style.setProperty("--margin-card", updateVarMargins)
                return 0
            })

            // set linespacing for every line
            let divLineStyle = document.getElementsByClassName("cardLine")
            Object.values(divLineStyle).map(item => {
                item.style.setProperty("--margin-card", margins.line_spacing_left + 'px ')
                return 0
            })
        }, 50)
    }
}

// Order the cardArray with the priority
export function prioritySort(newDataSorting, lemma, imgType, imgStyle, priorityOrder) {
    let count = 0
    for (let item of newDataSorting) {
        if (item.voice_human === lemma) {
            count++
        }
    }
    newDataSorting
        .sort((a, b) => {
            return a.lexical_expr - b.lexical_expr
        })
    let arrayInnerLemma, arrayOuterLemma
    if (count === newDataSorting.length) {
        arrayInnerLemma = newDataSorting
        arrayOuterLemma = []
    } else {
        arrayInnerLemma = newDataSorting.slice(0,count - 1)
        arrayOuterLemma = newDataSorting.slice(count - 1)
    }
    for (let i = 0; i < arrayInnerLemma.length; i++) {
        arrayInnerLemma[i].tmpOrder = {color: '', type: ''}
        arrayInnerLemma[i].finalOrder = ''
        if (arrayInnerLemma[i].imgcolor === imgType) {
            arrayInnerLemma[i].tmpOrder.color = '1'
        } else {
            arrayInnerLemma[i].tmpOrder.color = '0'
        }
        if (arrayInnerLemma[i].idstyle === imgStyle) {
            arrayInnerLemma[i].tmpOrder.type = '1'
        } else {
            arrayInnerLemma[i].tmpOrder.type = '0'
        }
        for (let j = 0; j < priorityOrder.length; j++) {
            arrayInnerLemma[i].finalOrder += arrayInnerLemma[i].tmpOrder[priorityOrder[j].text]
        }
    }
    arrayInnerLemma
        .sort((a, b) => {
            return parseInt(b.finalOrder,10) - parseInt(a.finalOrder,10)
        })
    for (let i = 0; i < arrayInnerLemma.length; i++) {
        delete arrayInnerLemma[i].tmpOrder
        delete arrayInnerLemma[i].finalOrder
    }
    let finalArray = arrayInnerLemma.concat(arrayOuterLemma)
    return finalArray
}

export function setNavBar(mode) {
    let navbarIcon = document.getElementById("navbar-icon")
    let navbarSpeak = document.getElementById("navbar-speak")
    if (mode === false) {
        navbarIcon.classList.add("viewMode")
        navbarSpeak.classList.remove("viewMode")
    } else {
        navbarIcon.classList.remove("viewMode")
        navbarSpeak.classList.add("viewMode")
    }
}

// Refresh token of the current user
export function refreshToken() {
    var myInit = {
        method: 'POST',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${sessionStorage.getItem('jwt')}`
        })
    }

    fetch(window.env.GraphQLRefreshToken, myInit)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            store.dispatch(jwtFetchDataSuccess(data))
        })
        .catch((error) => {
            console.log(error);
        })
}

export function validMail(item) {
    let regex = RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)
    return regex.test(item)
}
