import { FETCH_JWT_DATA, FETCH_JWT_DATA_ERROR, FETCH_JWT_DATA_LOADING, JWT_EXPIRED_OR_INVALID } from '../constants'
import { isValidObject } from '../functionUtils'

export function jwtHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_JWT_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function jwtIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_JWT_DATA_LOADING:
            return action.isLoading
        default:
            return state
    }
}

export function jwtExpiredOrInvalid(state = false, action) {
    switch (action.type) {
        case JWT_EXPIRED_OR_INVALID:
            return action.jwtError
        default:
            return state
    }
}

export function jwt(state = null, action) {
    switch (action.type) {
        case FETCH_JWT_DATA:
            if (isValidObject(action.jwt)) {
                sessionStorage.setItem('jwt', action.jwt.token)
                return action.jwt.token
            } else {
                sessionStorage.setItem('jwt', action.jwt)
                return action.jwt
            }

        default:
            return state
    }
}
