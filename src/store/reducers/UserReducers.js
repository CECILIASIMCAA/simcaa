import {
  FETCH_USER_DATA,
  FETCH_USER_REAL_DATA,
  FETCH_USER_DATA_TOHOME,
  FETCH_USER_DATA_ERROR,
  FETCH_USER_DATA_LOADING
} from "../constants";

export function userHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_USER_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function userIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_USER_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function userToHome(state = false, action) {
    switch (action.type) {
        case FETCH_USER_DATA_TOHOME:
            return action.toHome

        default:
            return state
    }
}

export function user(state = {}, action) {
    switch (action.type) {
        case FETCH_USER_DATA:
            return action.user

        default:
            return state
    }
}

export function userReal(state = {}, action) {
    switch (action.type) {
        case FETCH_USER_REAL_DATA:
            return action.user

        default:
            return state
    }
}
