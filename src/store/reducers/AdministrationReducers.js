//gestisce l'apertura e chiusura delle modali di aggiunta utente a team e creazione utente
export function modalReducer(state = { }, action) {
    let newState = null
    switch (action.type) {
        case "OPEN_MODAL_CREATE_USR":
          newState = Object.assign({}, state,{
            openModal: true,
            modalContent: "create_usr",
            toTeam: action.toTeam
          });
          return newState
        case "OPEN_MODAL_ADD_USR":
          newState = Object.assign({}, state,{
            openModal: true,
            modalContent: "add_usr",
            toTeam: action.toTeam
          });
          return newState
        case "OPEN_MODAL_MANAGE_TEAM":
          newState = Object.assign({}, state,{
            openModal: true,
            modalContent: "manage_team",
            toTeam: action.toTeam
          });
          return newState
        case "OPEN_MODAL_EDIT_USER_PARAMS":
          newState = Object.assign({},state,{
            openModal: true,
            modalContent: "edit_user_params",
            toUser: action.toUser
          })
          return newState
        case "HIDE_MODAL":
          newState = Object.assign({}, state,{
            openModal: false,
            modalContent: "none",
            toTeam: -1
          });
          return newState
        case "OPEN_MODAL_CREATE_TEAM":
          newState = Object.assign({}, state,{
            openModal: true,
            modalContent: "create_team"
          })
          return newState
        default:
            return state
    }
}

//gestisce salvataggio di dati sullo store per l'amministrazione
export function adminReducer(state = {roles: [], teams: [], user_teams: [], user_to_add: []}, action){
  let newState=null
  switch (action.type){
    case "SAVE_ADMIN_ROLES":
      newState = Object.assign({}, state,{
        roles: action.roles,
        teams: state.teams,
        user_teams: state.user_teams,
        user_to_add: state.user_to_add
      });
      return newState
    case "SAVE_ADMIN_TEAMS":
      newState = Object.assign({}, state,{
        roles: state.roles,
        teams: action.teams,
        user_teams: state.user_teams,
        user_to_add: state.user_to_add
      });
      return newState
    case "SAVE_ADMIN_USER_TEAMS":
      newState = Object.assign({}, state,{
        roles: state.roles,
        teams: state.teams,
        user_teams: action.user_teams,
        user_to_add: state.user_to_add
      });
      return newState
    case "SAVE_ADMIN_USER_TO_ADD":
      newState = Object.assign({}, state,{
        roles: state.roles,
        teams: state.teams,
        user_teams: state.user_teams,
        user_to_add: action.user_to_add
      });
      return newState
    default:
      return state
  }
}
