import { FETCH_STYLE_DATA, FETCH_STYLE_DATA_ERROR, FETCH_STYLE_DATA_LOADING } from '../constants'

export function styleDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_STYLE_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function styleDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_STYLE_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function styleData(state = [], action) {
    switch (action.type) {
        case FETCH_STYLE_DATA:
            return action.style

        default:
            return state
    }
}
