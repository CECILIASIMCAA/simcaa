import {
  FETCH_USER_DATA,
  FETCH_USER_REAL_DATA,
  FETCH_USER_DATA_TOHOME,
  FETCH_USER_DATA_ERROR,
  FETCH_USER_DATA_LOADING
} from "../constants";
import { closeModal } from '../actions/AdministrationActions'
import { resetDataFlag } from '../storeFunctionUtils'
import { apolloFetch } from '../apolloFetchUtils'

export function userFetchData(noInit = true, isLogin = true) {
    return (dispatch) => {
        if (noInit) {
            let initUser = {}
            dispatch(userFetchDataSuccess(initUser))
        }

        dispatch(userIsLoading(true))
        const token = sessionStorage.getItem('jwt')

        fetch(window.env.GraphQLCurrentUser, {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response
            })
            .then((response) => response.json())
            .then((user) => {
                user.user_permissions = JSON.parse(user.user_permissions)
                dispatch(userFetchDataSuccess(user))
                dispatch(userIsLoading(false))
                dispatch(userHasErrored(false))
                if (isLogin) {
                    dispatch(userToHome(true))
                    dispatch(resetDataFlag())
                }
            })
            .catch(() => dispatch(userHasErrored(true)))
    }
}

export function impersonateUser(id) {
    return (dispatch) => {
        let query = `
            query FetchUserToImpersonate {
                caa_users(id: ${id}) {
                    data {
                        id
                        name
                        user
                        email
                        user_permissions
                        organization
                        link_web
                        status
                        last_login
                        role_id
                        idstaff
                        created_at
                        updated_at
                    }
                }
            }
        `
        dispatch(userHasErrored(false))
        dispatch(userIsLoading(true))
        apolloFetch({query})
            .then(data => {
                    dispatch(userIsLoading(false))
                    if (data.data.caa_users.errors) {
                        dispatch(userHasErrored(true))
                    } else {
                        dispatch(userHasErrored(false))
                        let user = data.data.caa_users.data[0]
                        user.user_permissions = JSON.parse(user.user_permissions)
                        dispatch(userFetchDataSuccess(user))
                        dispatch(closeModal())
                    }
                })
            .catch(error => dispatch(userHasErrored(true)))
    }
}

export function userToHome(bool = false) {
    return (dispatch) => {
        let dispatchVar = {
            type: FETCH_USER_DATA_TOHOME,
            toHome: bool
        }
        dispatch(dispatchVar)
    }
}

export function userHasErrored(bool) {
    return {
        type: FETCH_USER_DATA_ERROR,
        hasErrored: bool
    }
}

export function userIsLoading(bool) {
    return {
        type: FETCH_USER_DATA_LOADING,
        isLoading: bool
    }
}

export function userFetchDataSuccess(user) {
    return {
        type: FETCH_USER_DATA,
        user
    }
}

export function userRealFetchDataSuccess(user) {
    return {
        type: FETCH_USER_REAL_DATA,
        user
    }
}
