import { FETCH_CLASS_DATA, FETCH_CLASS_DATA_ERROR, FETCH_CLASS_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth } from '../apolloFetchUtils'

export function classDataFetchData(fetchQuery, limit = 30, page = 1) {
    return (dispatch, getState) => {

        dispatch(classDataIsLoading(true))
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query allClasses {
                class(limit: ${limit}, page: ${page}) {
                    data {
                        id
                        descclass
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(classDataFetchDataSuccess(data.data.class.data))
                dispatch(classDataIsLoading(false))
                if (data.data.class.error) {
                    dispatch(classDataHasErrored(true))
                } else {
                    dispatch(classDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(classDataIsLoading(false))
                dispatch(classDataHasErrored(true))
            })
    }
}

export function classDataHasErrored(bool) {
    return {
        type: FETCH_CLASS_DATA_ERROR,
        hasErrored: bool
    }
}

export function classDataIsLoading(bool) {
    return {
        type: FETCH_CLASS_DATA_LOADING,
        isLoading: bool
    }
}

export function classDataFetchDataSuccess(classData) {
    return {
        type: FETCH_CLASS_DATA,
        class: classData
    }
}
