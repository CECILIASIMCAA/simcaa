import { TYPO_SELECTED_IMGSIZE,  } from '../constants'

export function dispatchTypoImgSize(size = 'small', value = null) {
    return (dispatch) => {
        if (size) {
            dispatch(selectedImgSize(size))
        } else {
            dispatch(selectedImgSize('small'))
        }
    }
}

export function selectedImgSize(size) {
    return {
        type: TYPO_SELECTED_IMGSIZE,
        size
    }
}
