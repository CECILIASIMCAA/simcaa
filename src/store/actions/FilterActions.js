import { FILTER_PROJECT_VALUE, FILTER_PROJECT_MODE, FILTER_PROJECT_PAGINATION_PAGE } from '../constants'

export function dispatchFilter(mode = null, value = null) {
    return (dispatch) => {
        if (mode) {
            dispatch(filterSetMode(mode))
        }
        if (value) {
            dispatch(filterSetValue(value))
        }
    }
}

export function filterSetMode(mode) {
    return {
        type: FILTER_PROJECT_MODE,
        mode
    }
}

export function filterSetValue(value = 1) {
    return {
        type: FILTER_PROJECT_VALUE,
        value
    }
}

export function setPaginationSelectedPage(value = 1) {
    return {
        type: FILTER_PROJECT_PAGINATION_PAGE,
        value
    }
}
