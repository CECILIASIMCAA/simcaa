import { FETCH_PROFILE_DATA, FETCH_PROFILE_DATA_ERROR, FETCH_PROFILE_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth, apolloFetch } from '../apolloFetchUtils'

export function profileDataFetchData(fetchQuery = null, limit = 30, page = 1) {
    return (dispatch, getState) => {

        dispatch(profileDataIsLoading(true))
        let state = getState()
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query allProfiles {
                profile_team(profile_user_id: ${state.user.id}, profile_team_id: ${state.filterValue}, limit: ${limit}, page: ${page}) {
                    data {
                        id
                        profile_name
                        profile_system
                        profile_conf
                        profile_user_id
                        user_name
                        team_name
                        profile_team_id
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(profileDataFetchDataSuccess(data.data.profile_team.data))
                dispatch(profileDataIsLoading(false))
                if (data.data.profile_team.error) {
                    dispatch(profileDataHasErrored(true))
                } else {
                    dispatch(profileDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(profileDataIsLoading(false))
                dispatch(profileDataHasErrored(true))
            })
    }
}

export function profileDataCreateData(mutationQuery) {
    return (dispatch, getState) => {

        dispatch(profileDataIsLoading(true))
        let query = mutationQuery

        apolloFetch({ query })
            .then((data) => {
                dispatch(profileDataIsLoading(false))
                if (data.data.createCaaProfile.error) {
                    dispatch(profileDataHasErrored(true))
                } else {
                    dispatch(profileDataHasErrored(false))
                    dispatch(profileDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(profileDataIsLoading(false))
                dispatch(profileDataHasErrored(true))
            })
    }
}

export function profileDataUpdateData(mutationQuery) {
    return (dispatch, getState) => {

        dispatch(profileDataIsLoading(true))
        let query = mutationQuery

        apolloFetch({ query })
            .then((data) => {
                dispatch(profileDataIsLoading(false))
                if (data.data.updateCaaProfile.error) {
                    dispatch(profileDataHasErrored(true))
                } else {
                    dispatch(profileDataHasErrored(false))
                    dispatch(profileDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(profileDataIsLoading(false))
                dispatch(profileDataHasErrored(true))
            })
    }
}

export function profileDataDeleteData(id) {
    return (dispatch, getState) => {

        dispatch(profileDataIsLoading(true))
        let query = `
            mutation deleteProfile {
                deleteCaaProfile(id: ${id}) {
                    id
                }
            }
        `
        apolloFetch({ query })
            .then((data) => {
                dispatch(profileDataIsLoading(false))
                if (data.data.deleteCaaProfile.error) {
                    dispatch(profileDataHasErrored(true))
                } else {
                    dispatch(profileDataHasErrored(false))
                    dispatch(profileDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(profileDataIsLoading(false))
                dispatch(profileDataHasErrored(true))
            })
    }
}

export function profileDataHasErrored(bool) {
    return {
        type: FETCH_PROFILE_DATA_ERROR,
        hasErrored: bool
    }
}

export function profileDataIsLoading(bool) {
    return {
        type: FETCH_PROFILE_DATA_LOADING,
        isLoading: bool
    }
}

export function profileDataFetchDataSuccess(profileData) {
    return {
        type: FETCH_PROFILE_DATA,
        profile: profileData
    }
}
