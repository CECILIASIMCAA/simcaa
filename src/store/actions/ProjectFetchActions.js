import { FETCH_PROJECT_DATA,FETCH_PROJECT_DATA_TOTAL, FETCH_PROJECT_DATA_ERROR, FETCH_PROJECT_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth, apolloFetch } from '../apolloFetchUtils'

export function projectDataFetchData(fetchQuery = null, limit = 15, page = 1, searchValue = null) {
    return (dispatch, getState) => {

        dispatch(projectDataIsLoading(true))
        let state = getState()
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            let variablesQuery = 'limit: ' + limit + ', page: ' + page + ','
            switch (state.filterMode) {
                case 'private':
                    variablesQuery += ' proj_owner: ' + state.user.id
                break;
                case 'team':
                    variablesQuery += ' team_id: ' + state.filterValue
                break;
                case 'shared':
                    variablesQuery += ' team_id: ' + state.filterValue + ', shared: ' + state.user.id
                break;
                default:
            }
            variablesQuery = searchValue ? variablesQuery + ' searchName: "' + searchValue + '"': variablesQuery

            query = `
            query allProjects {
                query_view_team(${variablesQuery}) {
                        total
                        data {
                            proj_id
                            proj_name
                            proj_owner
                            proj_share
                            proj_note
                            proj_profile
                            proj_layout
                            proj_blocked
                            user_name
                            team_name
                            team_id
                            updated_at
                        }
                    }
                }`
        }

        apolloFetch({ query })
            .then((data) => {
                dispatch(projectDataFetchDataSuccess(data.data.query_view_team.data))
                dispatch(projectDataIsLoading(false))
                if (data.data.query_view_team.error) {
                    dispatch(projectDataHasErrored(true))
                } else {
                    dispatch(projectDataFetchDataTotal(data.data.query_view_team.total))
                    dispatch(projectDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(projectDataHasErrored(true))
                dispatch(projectDataIsLoading(false))
            })
    }
}

export function projectDataCreateData(mutationQuery) {
    return (dispatch, getState) => {

        dispatch(projectDataIsLoading(true))
        let query = mutationQuery

        apolloFetch({ query })
            .then((data) => {
                dispatch(projectDataIsLoading(false))
                if (data.data.createCaaProject.error) {
                    dispatch(projectDataHasErrored(true))
                } else {
                    dispatch(projectDataHasErrored(false))
                    dispatch(projectDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(projectDataIsLoading(false))
                dispatch(projectDataHasErrored(true))
            })
    }
}

export function projectDataUpdateData(mutationQuery) {
    return (dispatch, getState) => {

        dispatch(projectDataIsLoading(true))
        let query = mutationQuery

        apolloFetch({ query })
            .then((data) => {
                dispatch(projectDataIsLoading(false))
                if (data.data.updateCaaProject.error) {
                    dispatch(projectDataHasErrored(true))
                } else {
                    dispatch(projectDataHasErrored(false))
                    dispatch(projectDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(projectDataIsLoading(false))
                dispatch(projectDataHasErrored(true))
            })
    }
}

export function projectDataDeleteChapterData(id, deleteProject = false) {
    return (dispatch, getState) => {

        dispatch(projectDataIsLoading(true))
        let query = `
        query chaptersToDelete {
            chapters(caa_project_id: ${id}, limit: 100) {
                data {
                    id
                }
            }
        }
        `
        apolloFetch({ query })
            .then((data) => {
                if (data.data.chapters.error) {
                    dispatch(projectDataIsLoading(false))
                    dispatch(projectDataHasErrored(true))
                } else {
                    if (data.data.chapters.data.length > 0) {
                        let idList = ''
                        for (let i = 0; i < data.data.chapters.data.length; i++) {
                            idList += data.data.chapters.data[i].id + ' '
                        }
                        query = `
                        mutation chapterToDelete {
                            deleteCaaChapter(idToDelete: "${idList}") {
                                id
                            }
                        }`
                        apolloFetch({ query })
                        .then((data) => {
                            if (data.data.deleteCaaChapter.error) {
                                dispatch(projectDataIsLoading(false))
                                dispatch(projectDataHasErrored(true))
                            } else {
                                dispatch(projectDataDeleteData(id))
                            }
                        })
                        .catch((error) => {
                            dispatch(projectDataIsLoading(false))
                            dispatch(projectDataHasErrored(true))
                        })
                    } else {
                        if (deleteProject === true) {
                            dispatch(projectDataDeleteData(id))
                        }
                    }
                }
            })
            .catch((error) => {
                dispatch(projectDataIsLoading(false))
                dispatch(projectDataHasErrored(true))
            })
    }
}

export function projectDataDeleteData(id) {
    return (dispatch, getState) => {

        dispatch(projectDataIsLoading(true))
        let query = `
        mutation deleteProject {
            deleteCaaProject(id: ${id}) {
                id
            }
        }
        `
        apolloFetch({ query })
            .then((data) => {
                if (data.data.deleteCaaProject.error) {
                    dispatch(projectDataIsLoading(false))
                    dispatch(projectDataHasErrored(true))
                } else {
                    dispatch(projectDataHasErrored(false))
                    dispatch(projectDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(projectDataIsLoading(false))
                dispatch(projectDataHasErrored(true))
            })
    }
}

// Duplicate the current project
export function duplicateProject(project) {
    return (dispatch, getState) => {
        let proj_share = project.proj_share >= 0 ? -1 : project.proj_share
        let data = JSON.stringify({id: project.proj_id, proj_owner: getState().user.id, proj_team: getState().filterValue, proj_share})
        fetch(window.env.RestApiDuplicateProject, {
            method: 'POST',
            body: data,
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            if (!isNaN(data)) {
                dispatch(projectDataFetchData())
            }
        })
        .catch((error) => {
            console.log(error);
        })
    }
}

// Lock/unlock project
export function lockUnlockProject(id, value) {
    return (dispatch) => {

        dispatch(projectDataIsLoading(true))
        let query = `
        mutation lockUnlockProject {
            updateCaaProject(id: ${id}, proj_blocked: ${value}) {
                id
            }
        }
        `
        apolloFetch({ query })
            .then((data) => {
                if (data.data.updateCaaProject.error) {
                    dispatch(projectDataIsLoading(false))
                    dispatch(projectDataHasErrored(true))
                } else {
                    dispatch(projectDataHasErrored(false))
                    dispatch(projectDataFetchData())
                }
            })
            .catch((error) => {
                dispatch(projectDataIsLoading(false))
                dispatch(projectDataHasErrored(true))
            })
    }
}

export function projectDataHasErrored(bool) {
    return {
        type: FETCH_PROJECT_DATA_ERROR,
        hasErrored: bool
    }
}

export function projectDataIsLoading(bool) {
    return {
        type: FETCH_PROJECT_DATA_LOADING,
        isLoading: bool
    }
}

export function projectDataFetchDataSuccess(projectData) {
    return {
        type: FETCH_PROJECT_DATA,
        project: projectData
    }
}

export function projectDataFetchDataTotal(total) {
    return {
        type: FETCH_PROJECT_DATA_TOTAL,
        total
    }
}
