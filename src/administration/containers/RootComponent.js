import React, { Component,Fragment } from 'react'
import {Card, Button, Icon, Popup} from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { connect } from 'react-redux'
import {openModalCreateTeam, openModalCreateUser, openModalAddUser, openModalManageTeam} from '../../store/actions/AdministrationActions'

import Can from '../../containers/Permission'

class RootComponent extends Component{
  createUserInTeam(team_id){
    this.props.openModalCreateUser(team_id)
  }
  addUserToTeam(team_id){
    this.props.openModalAddUser(team_id)
  }
  manageTeam(team_id){
    this.props.openModalManageTeam(team_id)
  }
  createNewTeam(){
    this.props.openModalCreateTeam()
  }

  render(){
    const { t } = this.props
    let teamsLayout = this.props.teams.map((team, idx)=>{
      //aggiungo card di questo team
      return(
        <Card key={idx} id={idx}>
          <Card.Content>
            <Card.Header>
              {team.team_name}
            </Card.Header>
            <Card.Meta>
              {team.team_email}
            </Card.Meta>
            <Card.Description>
              {'Descrizione del team'}
            </Card.Description>

          </Card.Content>
          <Card.Content extra>
            <div className='ui three buttons'>
            <Button basic color="violet" onClick={()=>this.createUserInTeam(team.team_id)}>
              Crea nuovo utente
            </Button>
            <Can perform='can_manage_user' and='can_manage_team'>
              <Button basic color="blue" onClick={()=>this.addUserToTeam(team.team_id)}>
                Aggiungi utente
              </Button>
            </Can>
            <Button basic color="green" onClick={() => this.manageTeam(team.team_id)}>
              Gestisci
            </Button>
            </div>
          </Card.Content>
        </Card>
      )
    });
    let cardWidth = 0
    if(document.getElementById('0') !== null){
      cardWidth = document.getElementById('0').offsetWidth/2
    }
    //let cardWidth = document.getElementById('0').offsetWidth
    return (
      <Fragment>
        <Card.Group centered style={{ margin: "10px" }}>
          {teamsLayout}
          <Can perform="can_create_team">
            <Popup
              trigger={
                <Button
                  circular
                  icon={<Icon name="plus" size="big" />}
                  style={{
                    width: cardWidth + "px",
                    height: cardWidth + "px",
                    marginTop: cardWidth / 3 + "px",
                    marginLeft: cardWidth / 4 + "px"
                  }}
                  onClick={() => this.props.openModalCreateTeam()}
                />
              }
              content="Crea un nuovo team"
            />
          </Can>
        </Card.Group>
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
    return {
        user: state.user,
        teams: state.adminReducer.teams
    }
}
const mapDispatchToProps = (dispatch) => {
  return {
    openModalCreateUser: (team_id) => dispatch(openModalCreateUser(team_id)),
    openModalAddUser: (team_id) => dispatch(openModalAddUser(team_id)),
    openModalManageTeam: (team_id) => dispatch(openModalManageTeam(team_id)),
    openModalCreateTeam: () => dispatch(openModalCreateTeam())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(translate('translations') (RootComponent))
