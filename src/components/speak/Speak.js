import React, { Component } from 'react'
import { Icon, Popup } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import { translate } from 'react-i18next'
import SpeakOptions from './SpeakOptions'

class Speak extends Component {
    constructor(props) {
        super(props)
        this.state= {
            checked: false,
            nCard: 0,
            text: [],
            oldBorder: [],
            openOptions: false,
            isReading: false,

            // Variable speak
            volume: 0.5,
            rate: 0.75,
            pitch: 1,
            scansione: 0,
            evidenziatore: 'card',
            voice: 'Italian Female',
        }
        this.clickCard = this.clickCard.bind(this)
    }

    componentDidMount() {
        var interval = setInterval(() => {
            let nCard = document.getElementsByClassName('cardUI')
            
            if (nCard.length > 0) {
                // Remember old border
                let localOldBorder = Object.assign([], this.state.oldBorder)
                for (let i = 0; i < nCard.length; i++) {
                    let cardElement = document.getElementById('card-' + i)
                    if (cardElement) {
                        localOldBorder.push(cardElement.style.border)
                    }
                }

                // Add the click event on every card
                for (let i = 0; i < nCard.length; i++) {
                    document.getElementById('card-' + i).addEventListener('click', this.clickCard)
                }
                this.setState({nCard: nCard.length, oldBorder: localOldBorder})
                clearInterval(interval)
            }
        }, 100)
    }

    componentDidUpdate(prevProps, prevState) {
        let mode = Object.values(document.getElementById('navbar-speak').classList).indexOf('viewMode')
        if (mode === -1 && this.state.checked === true) {
            setTimeout(() => {
                mode = Object.values(document.getElementById('navbar-speak').classList).indexOf('viewMode')
                if (mode !== -1) {
                    this.selectOn()
                }
            }, 100)
        }

        let nCard = document.getElementsByClassName('cardUI')
        if (nCard.length > 0 && this.state.nCard !== nCard.length) {
            // Add the click event on every card
            for (let i = 0; i < nCard.length; i++) {
                let card = document.getElementById('card-' + i)
                if (card) {
                    card.addEventListener('click', this.clickCard)
                }
            }
            this.setState({nCard: nCard.length})
        }

        // Disable navbar if isReading
        if (prevState.isReading !== this.state.isReading) {
            document.getElementById('navbar-edit').classList.toggle('disabled')
        }
    }
    
    // Non entra nel for TODO: vedere se eliminare
    // componentWillUnmount() {
    //     let nCard = document.getElementsByClassName('cardUI')
    //     for (let i = 0; i < nCard.length; i++) {
    //         document.getElementById(this.state.evidenziatore + '-' + i).removeEventListener('click', this.clickCard)
    //     }
    // }

    // Handle della selezione delle card da leggere
    selectOn() {
        this.setState({checked: !this.state.checked}, () => {
            if (this.state.checked === false) {
                for (let i = 0; i < this.state.nCard; i++) {
                    document.getElementById('card-' + i).style.border = this.state.oldBorder[i]
                    document.getElementById('text-' + i).style.border = '1px solid rgba(34,36,38,.15)'
                }
                this.setState({text: []})
            }
        })
    }

    // Handle onclick sulle card per selezionarle
    clickCard(e) {
        let mode = Object.values(document.getElementById('navbar-speak').classList).indexOf('viewMode')
        if (mode === -1) {
            if (this.state.checked === false) {
                let text = e.currentTarget.getElementsByTagName('input')[0].value
                let id = e.currentTarget.id.substr(5)
                if (!window.responsiveVoice.isPlaying()) {
                    this.nextPlay([{'text': text, 'id': id}], 0)
                }
            } else {
                document.getElementById(e.currentTarget.id).style.border = '5px solid blue'
                let text = e.currentTarget.getElementsByTagName('input')[0].value
                let id = e.currentTarget.id.substr(5)
                let localText = this.state.text
                localText.push({'text': text, 'id': id})
        
                // delete all duplicates from the array
                let uniq = new Set(localText.map(e => JSON.stringify(e)))
                localText = Array.from(uniq).map(e => JSON.parse(e))
        
                this.setState({text: localText})
            }
        }
    }

    // ResponsiveVoice Speak
    nextPlay(text, i) {
        this.setState({isReading: true})
        if (text[i] && text[i].text !== '') {
            let card = document.getElementById( this.state.evidenziatore + '-' + text[i].id)
            let oldBorder = card.style.border

            // Speak changing the border
            window.responsiveVoice.speak(text[i].text.toLowerCase(), this.state.voice, {
                onstart: () => {
                    card.style.border = '5px solid green'
                },
                onend: () => {
                    card.style.border = oldBorder
                    if (this.state.isReading) {
                        setTimeout(() => {
                            this.nextPlay(text, i + 1)
                        }, this.state.scansione)
                    }
                },
                volume: this.state.volume,
                pitch: this.state.pitch,
                rate: this.state.rate
            })
        } else {
            this.setState({isReading: false})
        }
    }

    // Handle onclick play
    play() {
        if (this.state.isReading) {
            this.setState({isReading: false})
        } else {
            let localText = Object.assign([], this.state.text)
            if (localText.length === 0) {
                for (let i = 0; i < this.state.nCard; i++) {
                    localText.push({'text': document.getElementById('text-' + i).value, 'id': i})
                }
            } else {
                localText.sort((a, b) => {
                    return a.id - b.id
                })
            }
            this.nextPlay(localText, 0)
        }
    }

    // Handle open/close options panel
    openCloseOptions() {
        this.setState({openOptions: !this.state.openOptions})
    }

    // Save options
    saveOptions(volume, rate, pitch, scansione, evidenziatore, voice) {
        this.setState({volume, rate, pitch, scansione, evidenziatore, voice})
        this.openCloseOptions()
    }

    // MAIN RENDER
    render() {
        let { t } = this.props

        return (
            <div style={{'display': this.props.hidden}}>
                <Popup
                    trigger={<Icon name={this.state.isReading ? 'pause circle' : 'play circle'}
                        bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={this.play.bind(this)}
                    />}
                    content={t("TTS_PLAY")}
                />
                <Popup
                    trigger={<Icon name='cogs' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={this.openCloseOptions.bind(this)}
                        disabled={this.state.isReading}
                    />}
                    content={t("TTS_OPTIONS")}
                />
                <Popup
                    trigger={<Icon name={this.state.checked ? 'toggle on' : 'toggle off'} bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={this.selectOn.bind(this)}
                        disabled={this.state.isReading}
                    />}
                    content={t("TTS_SELECT")}
                />
                <SpeakOptions
                    open={this.state.openOptions}
                    handleOptions={this.openCloseOptions.bind(this)}
                    saveOptions={this.saveOptions.bind(this)}
                    volume={this.state.volume}
                    pitch={this.state.pitch}
                    rate={this.state.rate}
                    scansione={this.state.scansione}
                    evidenziatore={this.state.evidenziatore}
                    voice={this.state.voice}
                />
            </div>
        )
    }
}

export default translate('translations')(withRouter(Speak))
