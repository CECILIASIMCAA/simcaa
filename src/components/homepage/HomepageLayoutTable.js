import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Icon } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import NewProfileForm from '../../containers/homepage/NewProfileForm'
import Can from '../../containers/Permission'

class HomepageLayoutTable extends Component {

    // Call the props for delete the current project
    confirmDeleteProfile(id, e) {
        this.props.confirm(id)
    }

    render() {
        const { t } = this.props

        let allProfiles = this.props.profile
        allProfiles = allProfiles.map((item, index) => {
            return (
                <Table.Row key={index}>
                    <Table.Cell collapsing>{item.id}</Table.Cell>
                    <Table.Cell >{item.profile_name}</Table.Cell>
                    <Table.Cell>{item.user_name}</Table.Cell>
                    <Table.Cell>{item.team_name}</Table.Cell>
                    <Table.Cell collapsing textAlign='right' disabled={item.profile_user_id === this.props.user.id ? false : true} className='fix-display'>
                        <NewProfileForm
                            className='icon-pointer'
                            size='big'
                            edit='icon'
                            data={item.profile_conf}
                            id={item.id}
                            disabled={item.profile_user_id === this.props.user.id ? false : true}
                        />
                    <Can perform='can_manage_profile' or='can_delete_profile'>
                            <Icon name='trash' color='red' size='big'
                                className='icon-pointer'
                                disabled={item.profile_user_id === this.props.user.id ? false : true}
                                onClick={this.confirmDeleteProfile.bind(this, item.id)}
                            />
                    </Can>
                    </Table.Cell>
                </Table.Row>
            )
        })

        return (
            <Table celled striped color='blue'>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>{t("MAIN_TBL_NAME")}</Table.HeaderCell>
                        <Table.HeaderCell>{t("MAIN_TBL_PROFILEOWN")}</Table.HeaderCell>
                        <Table.HeaderCell>{t("MAIN_TBL_TEAMOWN")}</Table.HeaderCell>
                        <Table.HeaderCell>{t("MAIN_TBL_ACTIONS")}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {allProfiles}
                </Table.Body>
            </Table>
        )
    }
}

HomepageLayoutTable.propTypes = {
    user: PropTypes.object.isRequired,
    profile: PropTypes.array.isRequired,
    confirm: PropTypes.func.isRequired,
}

export default translate('translations')(HomepageLayoutTable)
