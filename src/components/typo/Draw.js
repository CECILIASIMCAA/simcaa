import React, { Component, Fragment } from 'react'
import { Modal, Button, Popup, Segment, Grid, Menu, Tab, Input, Divider } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { CirclePicker } from 'react-color'
import { Stage, Layer, Rect, Circle, Arrow, Line } from 'react-konva'

class Draw extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            color: {rectFill: '#ffffff', rectStroke: '#f44336', circleFill: '#ffffff', circleStroke: '#f44336', arrow: '#f44336', line: '#f44336'},
            stroke: {rect: 4, circle: 4, arrow: 10, line: 10},
            pointer: {pointerLength: 20, pointerWidth:  20},
            isDrawing: false,
            indexTab: 0,
        }
    }

    // Handle draw modal open/close
    openCloseModal(isDrawing, e) {
        if (typeof isDrawing === "boolean") {
            if (isDrawing) {
                this.setState({open: !this.state.open, isDrawing})
            } else {
                this.props.draw('reset', this.state.color)
                this.setState({isDrawing})
            }
        } else {
            this.setState({open: !this.state.open, indexTab: 0,
                color: {rectFill: '#ffffff', rectStroke: '#f44336', circleFill: '#ffffff', circleStroke: '#f44336', arrow: '#f44336', line: '#f44336'},
                stroke: {rect: 4, circle: 4, arrow: 10, line: 10},
                pointer: {pointerLength: 20, pointerWidth:  20},
            })
        }
    }

    // Handle change color
    handleColorChange(type, color, e) {
        let localColor = Object.assign({}, this.state.color)
        localColor[type] = color.hex
        this.setState({color: localColor})
    }

    // Handle stroke change
    handleStrokeChange(type, e) {
        let stroke = e.target.value.match(/\d/g)
        stroke = stroke ? parseInt(stroke.join("")) : ''
        let localStroke = Object.assign({}, this.state.stroke)
        localStroke[type] = stroke
        this.setState({stroke: localStroke})
    }

    // Handle pointer change
    handlePointerChange(type, e) {
        let pointer = e.target.value.match(/\d/g)
        pointer = pointer ? parseInt(pointer.join("")) : ''
        let localPointer = Object.assign({}, this.state.pointer)
        localPointer[type] = pointer
        this.setState({pointer: localPointer})
    }

    // Return to typo ready to draw
    goDraw(type, e) {
        let options
        switch (type) {
            case 'rect':
            case 'circle':
                options = {strokeColor: this.state.color[`${type}Stroke`], fillColor: this.state.color[`${type}Fill`], strokeWidth: this.state.stroke[type]}
                break;
            case 'arrow':
                options = {color: this.state.color[type], pointerLength: this.state.pointer.pointerLength, pointerWidth: this.state.pointer.pointerWidth, strokeWidth: this.state.stroke[type]}
                break;
            case 'line':
                options = {color: this.state.color[type], strokeWidth: this.state.stroke[type]}
                break;
            default:
                break;
        }
        this.props.draw(type, options)
        this.openCloseModal(true)
    }

    // Handle tab index change
    handleTabChange(e, data) {
        this.setState({indexTab: data.activeIndex})
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        let tabPane = []
        tabPane[0] = <Fragment>
            <Grid columns='equal'>
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKECOLOR")}
                            <Popup trigger={<Segment style={{backgroundColor: this.state.color.rectStroke}} className='colorPickerDraw' />} on='click'>
                                <Popup.Header>{t("TYPO_DRAW_COLORSELECT")}</Popup.Header>
                                <Popup.Content>
                                    <CirclePicker onChange={this.handleColorChange.bind(this, 'rectStroke')} color={this.state.color.rectStroke} />
                                </Popup.Content>
                            </Popup>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_FILLCOLOR")}
                            <Popup trigger={<Segment style={{backgroundColor: this.state.color.rectFill}} className='colorPickerDraw' />} on='click'>
                                <Popup.Header>{t("TYPO_DRAW_COLORSELECT")}</Popup.Header>
                                <Popup.Content>
                                    <CirclePicker onChange={this.handleColorChange.bind(this, 'rectFill')} color={this.state.color.rectFill} />
                                </Popup.Content>
                            </Popup>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKEWIDTH")}
                            <Input value={this.state.stroke.rect} onChange={this.handleStrokeChange.bind(this, 'rect')} />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Column width={16}>
                    <Segment basic textAlign='center'>
                        <Button primary onClick={this.goDraw.bind(this, 'rect')}>{t("TYPO_DRAW_RECT")}</Button>
                    </Segment>
                </Grid.Column>
            </Grid>
            <Divider horizontal>{t("TYPO_LBL_PREVIEW")}</Divider>
            <Stage width={500} height={150} className='typoDrawFixCenter'>
                <Layer>
                    <Rect
                        x={100}
                        y={30}
                        width={300}
                        height={100}
                        fill={this.state.color.rectFill}
                        stroke={this.state.color.rectStroke}
                        strokeWidth={this.state.stroke.rect}
                    />
                </Layer>
            </Stage>
        </Fragment>

        tabPane[1] =  <Fragment>
            <Grid columns='equal'>
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKECOLOR")}
                            <Popup trigger={<Segment style={{backgroundColor: this.state.color.circleStroke}} className='colorPickerDraw' />} on='click'>
                                <Popup.Header>{t("TYPO_DRAW_COLORSELECT")}</Popup.Header>
                                <Popup.Content>
                                    <CirclePicker onChange={this.handleColorChange.bind(this, 'circleStroke')} color={this.state.color.circleStroke} />
                                </Popup.Content>
                            </Popup>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_FILLCOLOR")}
                            <Popup trigger={<Segment style={{backgroundColor: this.state.color.circleFill}} className='colorPickerDraw' />} on='click'>
                                <Popup.Header>{t("TYPO_DRAW_COLORSELECT")}</Popup.Header>
                                <Popup.Content>
                                    <CirclePicker onChange={this.handleColorChange.bind(this, 'circleFill')} color={this.state.color.circleFill} />
                                </Popup.Content>
                            </Popup>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKEWIDTH")}
                            <Input value={this.state.stroke.circle} onChange={this.handleStrokeChange.bind(this, 'circle')} />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Column width={16}>
                    <Segment basic textAlign='center'>
                        <Button primary onClick={this.goDraw.bind(this, 'circle')}>{t("TYPO_DRAW_CIRCLE")}</Button>
                    </Segment>
                </Grid.Column>
            </Grid>
            <Divider horizontal>{t("TYPO_LBL_PREVIEW")}</Divider>
            <Stage width={500} height={150} className='typoDrawFixCenter'>
                <Layer>
                    <Circle
                        x={250}
                        y={80}
                        radius={50}
                        fill={this.state.color.circleFill}
                        stroke={this.state.color.circleStroke}
                        strokeWidth={this.state.stroke.circle}
                    />
                </Layer>
            </Stage>
        </Fragment>

        tabPane[2] = <Fragment>
            <Grid columns='equal'>
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKECOLOR")}
                            <Popup trigger={<Segment style={{backgroundColor: this.state.color.arrow}} className='colorPickerDraw' />} on='click'>
                                <Popup.Header>{t("TYPO_DRAW_COLORSELECT")}</Popup.Header>
                                <Popup.Content>
                                    <CirclePicker onChange={this.handleColorChange.bind(this, 'arrow')} color={this.state.color.arrow} />
                                </Popup.Content>
                            </Popup>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_POINTERWIDTH")}
                            <Input value={this.state.pointer.pointerWidth} onChange={this.handlePointerChange.bind(this, 'pointerWidth')} />
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                        {t("TYPO_DRAW_POINTERLENGTH")}
                            <Input value={this.state.pointer.pointerLength} onChange={this.handlePointerChange.bind(this, 'pointerLength')} />
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKEWIDTH")}
                            <Input value={this.state.stroke.arrow} onChange={this.handleStrokeChange.bind(this, 'arrow')} />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Column width={16}>
                    <Segment basic textAlign='center'>
                        <Button primary onClick={this.goDraw.bind(this, 'arrow')}>{t("TYPO_DRAW_ARROW")}</Button>
                    </Segment>
                </Grid.Column>
            </Grid>
            <Divider horizontal>{t("TYPO_LBL_PREVIEW")}</Divider>
            <Stage width={500} height={150} className='typoDrawFixCenter'>
                <Layer>
                    <Arrow
                        x={200}
                        y={80}
                        points={[-100,0,200,0]}
                        tension={1}
                        pointerLength={this.state.pointer.pointerLength}
                        pointerWidth={this.state.pointer.pointerWidth}
                        fill={this.state.color.arrow}
                        stroke={this.state.color.arrow}
                        strokeWidth={this.state.stroke.arrow}
                    />
                </Layer>
            </Stage>
        </Fragment>

        tabPane[3] = <Fragment>
            <Grid columns='equal'>
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKECOLOR")}
                            <Popup trigger={<Segment style={{backgroundColor: this.state.color.line}} className='colorPickerDraw' />} on='click'>
                                <Popup.Header>{t("TYPO_DRAW_COLORSELECT")}</Popup.Header>
                                <Popup.Content>
                                    <CirclePicker onChange={this.handleColorChange.bind(this, 'line')} color={this.state.color.line} />
                                </Popup.Content>
                            </Popup>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            {t("TYPO_DRAW_STROKEWIDTH")} <br />
                            <Input value={this.state.stroke.line} onChange={this.handleStrokeChange.bind(this, 'line')} />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Column width={16}>
                    <Segment basic textAlign='center'>
                        <Button primary onClick={this.goDraw.bind(this, 'line')}>{t("TYPO_DRAW_LINE")}</Button>
                    </Segment>
                </Grid.Column>
            </Grid>
            <Divider horizontal>{t("TYPO_LBL_PREVIEW")}</Divider>
            <Stage width={500} height={150} className='typoDrawFixCenter'>
                <Layer>
                    <Line
                        x={200}
                        y={80}
                        points={[-100,0,200,0]}
                        fill={this.state.color.line}
                        stroke={this.state.color.line}
                        strokeWidth={this.state.stroke.line}
                    />
                </Layer>
            </Stage>
        </Fragment>

        let panes = tabPane.map((item, index) => {
            return { menuItem: t(`TYPO_DRAW${index}_TAB`), render: () => <Tab.Pane>{item}</Tab.Pane> }
        })

        return(
            <Modal trigger={<Menu.Item
                                onClick={this.state.isDrawing ? this.openCloseModal.bind(this, false) : this.openCloseModal.bind(this)}
                                disabled={this.props.disabled}
                                style={this.state.isDrawing === true ? {backgroundColor: 'darkred', color: 'white'} : {color: 'black'}}
                                className={this.props.className}
                            >
                                {this.state.isDrawing ? t("TYPO_ENDSHAPES") : t("TYPO_DRAWSHAPES")}
                            </Menu.Item>}
                open={this.state.open}
            >
                <Modal.Header>{t("TYPO_DRAWSHAPES")}</Modal.Header>
                <Modal.Content>
                    <Tab panes={panes} activeIndex={this.state.indexTab} onTabChange={this.handleTabChange.bind(this)}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={this.openCloseModal.bind(this)}>{t("HEAD_BTN_CLOSE")}</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default translate('translations')(Draw)
