import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Progress, Grid, List, Icon } from 'semantic-ui-react'

class InitProgressBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            password: '',
        }
    }

    callLogin() {
        this.props.login(this.state.user, this.state.password)
    }

    render() {

        let list
        if (this.props.list && this.props.listProgress) {
            let listProgress = Object.assign(this.props.listProgress)
            listProgress = listProgress.map((item, index) => {
                return (
                    <List.Item key={index}>
                        <List.Content>
                            <Icon loading={item.loading} name={item.iconName} color={item.color}/>
                            {item.innerString}
                        </List.Content>
                    </List.Item>
                )
            })
            list = <List>{listProgress}</List>
        }

        return (
            <Grid
                textAlign='center'
                style={{ height: '100vh', margin: 'auto' }}
                verticalAlign='middle'
                columns={2}
            >
                <Grid.Column>
                    <Progress percent={this.props.percent} label={this.props.label} indicating />
                    {list}
                </Grid.Column>
            </Grid>
        )
    }

}

InitProgressBar.propTypes = {
    percent: PropTypes.number.isRequired,
    label: PropTypes.string,
    list: PropTypes.bool,
    listProgress: PropTypes.arrayOf(PropTypes.shape({
            iconName: PropTypes.string.isRequired,
            loading: PropTypes.bool.isRequired,
            color: PropTypes.string,
            innerString: PropTypes.string.isRequired
        })
    ),
}

export default InitProgressBar
